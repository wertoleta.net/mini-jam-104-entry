Wert.Bubble = function() {
	Wert.Bubble.super.constructor.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	let bubbleIndex = 1 + Math.floor(Math.random() * 6);
	let texture = sheet.textures['bubble_' + bubbleIndex];
	
	this.body = new PIXI.Sprite(texture);	
	this.body.anchor.set(0.5);
	
	this.SetRadius(16 + Math.floor(Math.random() * 17));
	
	this.scale.x = this.scale.y = 0;
	
	this.velocity = new Victor(0, 0);
	this.previousPosition = {x: 0, y: 0};
	this.speed = speed = new Victor(0, 0);
	
	this.addChild(this.body);
}

Extend(Wert.Bubble, PIXI.Container);

Wert.Bubble.prototype.body = null;
Wert.Bubble.prototype.radius = 32;
Wert.Bubble.prototype.area = Wert.Bubble.prototype.radius * Wert.Bubble.prototype.radius * Math.PI;
Wert.Bubble.prototype.velocity = null;
Wert.Bubble.prototype.radiusScale = 1;
Wert.Bubble.prototype.friction = new Victor(1.5, 1.5);
Wert.Bubble.prototype.topLine = (Wert.ViewportSize.max.height - Wert.ViewportSize.min.height) / 2;
Wert.Bubble.prototype.previousPosition = null;
Wert.Bubble.prototype.speed = null;
Wert.Bubble.prototype.minSpeed = 1;
Wert.Bubble.prototype.maxSpeed = new Victor(20, 20);
Wert.Bubble.prototype.upSpeed = 1;
Wert.Bubble.prototype.item = null;
Wert.Bubble.prototype.isEmergeable = true;
Wert.Bubble.prototype.canHandleItem = true;


Wert.Bubble.prototype.SetRadius = function(newRadius) {
	this.radius = newRadius;
	this.area = (this.radius * this.radius) * Math.PI;
	this.radiusScale = (this.radius * 2) / this.body.texture.width;
}

Wert.Bubble.prototype.SetArea = function(newArea) {
	this.SetRadius(Math.sqrt(newArea / Math.PI));
}

const halfPi = Math.PI / 2;
const quadPi = Math.PI / 4;

Wert.Bubble.prototype.Update = function() {
	Wert.Bubble.super.Update.call(this);
	
	let limit = new Victor(Math.min(this.maxSpeed.x, this.radius / 2), Math.min(this.maxSpeed.y, this.radius / 2));
	let velCur = this.velocity.clone();
	if (velCur.length() > limit.x)
	{
		velCur.normalize().multiply(limit);
	}
	
	this.position.x += velCur.x;
	this.position.y += velCur.y;
	
	this.velocity.divide(this.friction);
	
	this.velocity.y -= (this.upSpeed - (this.item == null ? 0 : this.item.weight));
	
	if (this.velocity.length() < this.minSpeed) {
		this.velocity.x = 0;
		this.velocity.y = 0;
	}
	
	this.speed.x = this.position.x - this.previousPosition.x;
	this.speed.y = this.position.y - this.previousPosition.y;
	let hAngle = this.speed.angle();
	let vAngle = this.speed.verticalAngle();
	let hAngleAbs = Math.abs(hAngle);
	let vAngleAbs = Math.abs(vAngle);
	let speedWeight = Math.max(0.0, Math.min(1.0, (this.speed.length() - 3.0) / 10.0));
	let xPower = (1 - (Math.min(Math.abs(halfPi - hAngleAbs), quadPi) / quadPi)) * speedWeight;
	let yPower = (1 - (Math.min(Math.abs(halfPi - vAngleAbs), quadPi) / quadPi)) * speedWeight;
	let skxAngle = Math.min(Math.abs(-halfPi - quadPi - hAngle), Math.abs(quadPi - hAngle));
	let skxPower = (1 - (Math.min(skxAngle, quadPi) / quadPi)) * speedWeight;
	let skyAngle = Math.min(Math.abs(-quadPi - hAngle), Math.abs(halfPi + quadPi - hAngle));
	let skyPower = (1 - (Math.min(skyAngle, quadPi) / quadPi)) * speedWeight;
	
	let targetScaleX = (1 - xPower * 0.5) * this.radiusScale;
	let targetScaleY = (1 - yPower * 0.5) * this.radiusScale;
	if (Math.abs(this.scale.x - targetScaleX) < 0.01) {
		this.scale.x = targetScaleX;
	}else {
		this.scale.x += (targetScaleX - this.scale.x) / 8;
	}
	if (Math.abs(this.scale.y - targetScaleY) < 0.01) {
		this.scale.y = targetScaleY;
	}else {
		this.scale.y += (targetScaleY - this.scale.y) / 8;
	}
	
	let targetSkewX = (skxPower - skyPower) * quadPi / 2;
	let targetSkewY = (skxPower - skyPower) * quadPi / 2;
	if (Math.abs(this.skew.x - targetSkewX) < 0.01) {
		this.skew.x = targetSkewX;
	}else {
		this.skew.x += (targetSkewX - this.skew.x) / 8;
	}
	if (Math.abs(this.skew.y - targetSkewY) < 0.01) {
		this.skew.y = targetSkewY;
	}else {
		this.skew.y += (targetSkewY - this.skew.y) / 8;
	}
	
	this.previousPosition.x = this.position.x;
	this.previousPosition.y = this.position.y;
	
	if (this.item != null)
	{
		if (this.item.radius <= this.radius)
		{
			this.item.position.x = this.position.x;
			this.item.position.y = this.position.y;
		}
		else 
		{
			this.position.set(this.item.position.x, this.item.position.y);
			this.velocity.x = 0;
			this.velocity.y = 0;
		}
		this.item.skew.x = this.skew.x;
		this.item.skew.y = this.skew.y;
	}
};

Wert.Bubble.prototype.CollapseWith = function(bubble2, collisionVector) {
	if (bubble2.item != null)
	{
		let item = bubble2.item;
		bubble2.item		= null;
		item.FreeFromBubble();
		
		if (this.item == null && this.canHandleItem)
		{
			this.item = item;
			this.velocity.x = 0;
			this.velocity.y = 0;
			item.CatchInBubble(this)
		}
	}
	
	
	let weight1 = this.area / (this.area + bubble2.area);
	this.position.x += collisionVector.x * (1 - weight1);
	this.position.y += collisionVector.y * (1 - weight1);
	
	if (this.item == null)
	{
		this.velocity.x = this.velocity.x * weight1 + bubble2.velocity.x * (1 - weight1);
		this.velocity.y = this.velocity.y * weight1 + bubble2.velocity.y * (1 - weight1);
	}
	this.SetArea(this.area + bubble2.area);
	
	if (!Wert.IsMuted)
	{
		PIXI.Loader.shared.resources["Pop"].data.currentTime = 0;
		PIXI.Loader.shared.resources["Pop"].data.play();
	}
};