Wert.Button = function(texture, callback) {
	Wert.Button.super.constructor.call(this, texture);
	this.callback = callback;
	
	this.sprite = new PIXI.Sprite(texture);
	this.sprite.anchor.set(0.5);
	this.addChild(this.sprite);
}

Extend(Wert.Button, PIXI.Container);

Wert.Button.prototype.callback = null;
Wert.Button.prototype.normalScale = 1;
Wert.Button.prototype.active = true;
Wert.Button.prototype.sprite = null;

Wert.Button.prototype.Update = function() {
	Wert.Button.super.Update.call(this);
	
	let bounds = this.getBounds();
	bounds.x += -Wert.CurrentStage.x;
	bounds.y += -Wert.CurrentStage.y;
	let m = Wert.CurrentStage.mouse;
	if (this.active && m.x >= bounds.x && m.x <= bounds.x + bounds.width && m.y >= bounds.y && m.y <= bounds.y + bounds.height)
	{
		this.scale.set(this.normalScale * 1.2);
		if (m.isPressed)
		{
			this.callback();
		}
	}
	else
	{
		this.scale.set(this.normalScale);
	}
	
};