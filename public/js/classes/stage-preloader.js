Wert.StagePreloader = function() {
	Wert.StagePreloader.super.constructor.call(this);
}

Extend(Wert.StagePreloader, Wert.Stage);

Wert.StagePreloader.prototype.Create = function() {
	Wert.StagePreloader.super.Create.call(this);
	
	PIXI.Loader.shared
	.add("Atlas", "./img/atlas.json")
	.add("Tileset", "./maps/tileset.png")
	.add("MapStart", "./maps/start.json")
	.add("Part1", "./maps/part1.json")
	.add("Part2", "./maps/part2.json")
	.add("Part3", "./maps/part3.json")
	.add("Part4", "./maps/part4.json")
	.add("MainMenu", "./maps/main-menu.json")
	.add("BackgroundMusic", "./audio/background.mp3")
	.add("Hit", "./audio/hit.wav")
	.add("Jump", "./audio/jump.wav")
	.add("Kill", "./audio/kill.wav")
	.add("Shoot", "./audio/shoot.wav")
	.add("Coin1", "./audio/coin1.wav")
	.add("Coin2", "./audio/coin2.wav")
	.add("Coin3", "./audio/coin3.wav")
	.add("Coin4", "./audio/coin4.wav")
	.load(this.PreloadingFinished);
};

Wert.StagePreloader.prototype.Update = function() {
	Wert.StagePreloader.super.Update.call(this);
};

Wert.StagePreloader.prototype.Resize = function(newSize) {	
	Wert.StagePreloader.super.Resize.call(this, newSize);
};

Wert.StagePreloader.prototype.PreloadingFinished = function() {
	PIXI.Loader.shared.resources["BackgroundMusic"].data.volume = 0.5;
	PIXI.Loader.shared.resources["BackgroundMusic"].data.loop = true;
	//PIXI.Loader.shared.resources["Death"].data.volume = 0.25;
        PIXI.Loader.shared.resources["Shoot"].data.volume = 0.5;
        PIXI.Loader.shared.resources["Coin1"].data.volume = 0.5;
        PIXI.Loader.shared.resources["Coin2"].data.volume = 0.2;
       	PIXI.Loader.shared.resources["Coin4"].data.volume = 0.2;
			
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	
	window.setTimeout(()=>{
		Wert.SwitchStage(new Wert.StageMainMenu())
	}, 500);
	;
};