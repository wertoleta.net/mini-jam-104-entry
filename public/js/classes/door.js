Wert.Door = function(data) {
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	let texture = sheet.textures[data.width > data.height ? 'lock-blue' : 'lock-blue2'];
	Wert.Door.super.constructor.call(this, texture);
	
	this.lines = [];
	
	this.position.x = data.x;
	this.position.y = data.y;
	this.scale.x = data.width / texture.width;
	this.scale.y = data.height / texture.height;
	this.lines.push({
		p1: new Victor(data.x, data.y),
		p2: new Victor(data.x + data.width, data.y)
	});
	this.lines.push({
		p1: new Victor(data.x, data.y + data.height),
		p2: new Victor(data.x + data.width, data.y + data.height)
	});
	this.lines.push({
		p1: new Victor(data.x, data.y),
		p2: new Victor(data.x, data.y + data.height)
	});
	this.lines.push({
		p1: new Victor(data.x + data.width, data.y),
		p2: new Victor(data.x + data.width, data.y + data.height)
	});
	
	for (let lineIndex = 0; lineIndex < this.lines.length; lineIndex++)
	{
		this.lines[lineIndex].type = 'lock';
		this.lines[lineIndex].door = this;
	}
}

Extend(Wert.Door, PIXI.Sprite);

Wert.Door.prototype.lines = null;