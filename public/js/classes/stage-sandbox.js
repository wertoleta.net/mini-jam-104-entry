Wert.StageSandbox = function() {
	Wert.StageSandbox.super.constructor.call(this);
    this.MapParts = [];
    this.LastMapPartStaticObjects = [];
    this.State = StageState.Game;
    this.EndLevelCountdown = this.EndLevelCountdownMax;
    this.LastLine = Wert.ViewportSize.min.height;

    this.BottomLine = -this.y + Wert.ViewportSize.min.height;
    this.TopLine = -this.y;

	let storedScore = null;
	try {
		storedScore = window.localStorage.getItem('score');
	} catch{}
	
	Wert.StageSandbox.prototype.TopScore = storedScore ? parseInt(storedScore) : Wert.StageSandbox.prototype.TopScore;
}

Extend(Wert.StageSandbox, Wert.Stage);

var DefaultBuilder;

StageState = {
    Game: 1,
    Emd: 2
};

Wert.StageSandbox.prototype.LastLine = 0;
Wert.StageSandbox.prototype.BottomLine = 0;
Wert.StageSandbox.prototype.TopLine = 0;
Wert.StageSandbox.prototype.MapParts = [];
Wert.StageSandbox.prototype.LastMapPart = null;
Wert.StageSandbox.prototype.LastMapPartStaticObjects = [];
Wert.StageSandbox.prototype.State = StageState.Game;
Wert.StageSandbox.prototype.EndLevelCountdownMax = 90;
Wert.StageSandbox.prototype.EndLevelCountdown = 90;
Wert.StageSandbox.prototype.ParticleLayer = null;
Wert.StageSandbox.prototype.StartTimer = 30;
Wert.StageSandbox.prototype.waterlineSpeed = 0.25;
Wert.StageSandbox.prototype.waterlinePosition = 0;
Wert.StageSandbox.prototype.lastLine = 0;
Wert.StageSandbox.prototype.TopScore = 0;
Wert.StageSandbox.prototype.Waypoints = 1;

Wert.StageSandbox.prototype.Create = function() {
	Wert.StageSandbox.super.Create.call(this);

    this.hero = new Wert.Hero({X: Wert.ViewportSize.min.width / 2, Y: Wert.ViewportSize.min.height - 96});

    this.foreground = new PIXI.Container();
    this.addChild(this.foreground);

    this.ParticleLayer = new PIXI.Container();
    this.addChild(this.ParticleLayer);

	this.phys = new Wert.RectangleSpace();

	this.addChild(this.phys);

    this.waterfalls = new PIXI.Container();
    this.addChild(this.waterfalls);

    this.waterlineSprite = new PIXI.AnimatedSprite(PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Water"]);
    this.waterlineSprite.x = -100;
    this.waterlineSprite.animationSpeed = 0.3;
    this.waterlineSprite.play();
    this.addChild(this.waterlineSprite);

    this.waterline = new PIXI.TilingSprite(PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Water"][0], 800, 32);
    this.waterlinePosition = this.BottomLine + 64;
    this.waterline.y = this.waterlinePosition;

    var sprite = new PIXI.Sprite(PIXI.Texture.WHITE);
    sprite.tint = 0x00B7EF; //Change with the color wanted
    sprite.width = 800;
    sprite.height = 544;
    sprite.y = 32;
    this.waterline.addChild(sprite);

    this.waterline.alpha = 0.5;
    this.addChild(this.waterline);

    this.gui = new PIXI.Container();
    this.addChild(this.gui);

    this.heartsContainer = new PIXI.Container();
    this.gui.addChild(this.heartsContainer);

    for (var i = 0; i < this.hero._maxHealth; i++)
    {
        var hearth = new PIXI.Sprite(PIXI.Loader.shared.resources["Atlas"].spritesheet.textures["Hearth"]);
        hearth.x = 8 + (hearth.width  + 4) * i;
        this.heartsContainer.addChild(hearth);
    }
    this.UpdateHearts();

    this.phys.AddBody(this.hero);
  
    this.AddMapPart();

	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	
	this.homeButton = new Wert.Button(sheet.textures['buttons/home'], function(){
		Wert.SwitchStage(new Wert.StageMainMenu())
	});
	this.homeButton.normalScale = 0.5;
	
	this.soundButton = new Wert.Button(sheet.textures[Wert.IsMuted ? 'buttons/sound-off' : 'buttons/sound-on'], function(){
		if (!Wert.SoundPlayed)
		{
			PIXI.Loader.shared.resources["BackgroundMusic"].data.play();
			Wert.SoundPlayed = true;
			Wert.IsMuted = false;
		}
		else
		{
			Wert.IsMuted = !Wert.IsMuted;
			PIXI.Loader.shared.resources["BackgroundMusic"].data.muted = Wert.IsMuted;
		}
		this.sprite.texture = sheet.textures[Wert.IsMuted ? 'buttons/sound-off' : 'buttons/sound-on'];
	});
	this.soundButton.normalScale = 0.5;

    this._scoreText = new PIXI.Text("Score: 0", {fontFamily : 'Arial', fontSize: 32, fill : 0xffffff, align : 'center'});
    this._scoreText.y = 54;
    this._scoreText.x = 8;

    this._topScoreText = new PIXI.Text("Top score: 0", {fontFamily : 'Arial', fontSize: 24, fill : 0xffffff, align : 'center'});
    this._topScoreText.y = 32;
    this._topScoreText.x = 8;


	this.gui.addChild(this.homeButton);	
	this.gui.addChild(this.soundButton);	
	this.gui.addChild(this._scoreText);	
	this.gui.addChild(this._topScoreText);	
};

Wert.StageSandbox.prototype.Update = function() {

	Wert.StageSandbox.super.Update.call(this);

    this.hero._score += (Math.floor(-this.TopLine)  - this.lastLine);
    this.lastLine = Math.floor(-this.TopLine);
    this._scoreText.text = "Score: " + this.hero._score;

    if (this.hero._score > this.Waypoints * 5000)
    {
        this.Waypoints++;
        this.hero._health = Math.min(this.hero._maxHealth, this.hero._health + 1);
        this.UpdateHearts();
    }

    Wert.StageSandbox.prototype.TopScore = Math.max(Wert.StageSandbox.prototype.TopScore, this.hero._score);
    this._topScoreText.text = "Top score: " + Wert.StageSandbox.prototype.TopScore;
	
	this.homeButton.x = Wert.ViewportSize.min.width - this.homeButton.width / 2 - 8;
	this.homeButton.y = Wert.ViewportSize.min.height - this.homeButton.height / 2 - 16;
	
	this.soundButton.x = this.soundButton.width / 2 + 16;
	this.soundButton.y = Wert.ViewportSize.min.height - this.soundButton.height / 2 - 16;
    
    if (this.waterline.texture != this.waterlineSprite.texture)
    {
        this.waterline.texture = this.waterlineSprite.texture;
    }

    this.waterlinePosition = Math.max(this.TopLine + 200, Math.min(this.waterlinePosition - this.waterlineSpeed - this.lastLine / 10000, this.waterlinePosition + (this.BottomLine - this.waterlinePosition + 32) / 8.0));
    this.waterline.y = Math.floor(this.waterlinePosition);

    switch (this.State)
    {
        case StageState.Game:
            var target = Math.max(this.y, Wert.ViewportSize.min.height / 2 - this.hero.y);
            this.y += (target - this.y) / 8.0;
            this.gui.y = -this.y + 8;

            this.BottomLine = -this.y + Wert.ViewportSize.min.height;
            this.TopLine = -this.y;

            if (this.TopLine < this.LastLine - 1)
            {
                this.AddMapPart();
            }
            for (var i = this.MapParts.length - 1; i >= 0; i--)
            {
                var mapPart = this.MapParts[i];
                if (-mapPart.y < this.y - Wert.ViewportSize.min.height)
                {
                    this.MapParts.splice(i, 1);
                    this.foreground.removeChild(mapPart);
                    var bodies = mapPart.Bodies;
                    for (var j = 0; j < bodies.length; j++)
                    {
                        this.phys.RemoveBody(bodies[j]);
                    }
                }
            }
            break;
        case StageState.EndLevel:
            this.EndLevelCountdown--;
            if (this.EndLevelCountdown < 0)
            {
                let storedScore = null;
                try {
                    storedScore = window.localStorage.getItem('score');
                }catch{}
                let maxScore = storedScore ? Math.max(parseInt(storedScore), this.hero._score) : this.hero._score;
                try
                {
                    window.localStorage.setItem('score', maxScore);
                }catch{
                    Wert.StageSandbox.prototype.TopScore = maxScore;
                }
                Wert.SwitchStage(new Wert.StageSandbox());
            }
            break;
        }
};

Wert.StageSandbox.prototype.Resize = function(newSize) {	
	Wert.StageSandbox.super.Resize.call(this, newSize);
};

Wert.StageSandbox.prototype.MyBuilder =  function(meta){
    switch (meta.type)
    {
        case "Wall":
            var wall = new Wert.RectangleBody(
                {
                    Position: {X: meta.x, Y: meta.y + Wert.CurrentStage.LastLine}, 
                    Size: {Width: meta.width, Height: meta.height}
                }, 
                Wert.MovableType.Static);
            if (meta.properties)
            {
                meta.properties.forEach(property => {
                    if (property.name == "sides")
                    {
                        wall.SetSolidSides(property.value);
                    }
                });
            }
            Wert.CurrentStage.LastMapPartStaticObjects.push(wall);
            Wert.CurrentStage.phys.AddBody(wall);
            break;
        case "EnemySpawn":
            let chance = 0.5;
            if (meta.properties)
            {
                meta.properties.forEach(property => {
                    if (property.name == "probability")
                    {
                        chance = property.value / 100;
                    }
                });
            }
            var enemyClass = Math.random() <= chance ?  Wert.Blobbo : null;
            if (enemyClass != null)
            {
                var x = meta.x + Math.random() * (meta.width - enemyClass.prototype._defaultSize.Width);
                var enemy = new enemyClass(new Wert.FloatPoint(x, meta.y + Wert.CurrentStage.LastLine));
                Wert.CurrentStage.phys.AddBody(enemy);
            }
            break;
        case "WaterFall":
            var waterfall = new Wert.WaterFall();
            waterfall.x = meta.x - 50;
            waterfall.y = meta.y + Wert.CurrentStage.LastLine;
            waterfall.height = meta.height + 2;
            waterfall.scale.x = 0.5 * meta.width / 32;
            Wert.CurrentStage.waterfalls.addChild(waterfall);
            break;
        case "RuneLine":
            var polyline = meta.polyline == undefined ? meta.polygon : meta.polyline;
            if (polyline.length >= 2)
            {
                var d = 0;
                var currentIndex = 0;
                for (var currentIndex = 0; currentIndex < polyline.length - 1; currentIndex++)
                {
                    var cur = polyline[currentIndex];
                    var next = polyline[currentIndex + 1];
                    var vect = {x: next.x - cur.x, y: next.y - cur.y};
                    var dist = Math.sqrt(Math.pow(vect.x, 2) + Math.pow(vect.y, 2));
                    while (d < dist)
                    {
                        if (Math.random() < 0.25)
                        {
                            var k = (d / dist)
                            var x = meta.x + cur.x + vect.x * k;
                            var y = meta.y + cur.y + vect.y * k;
                            var rune = new Wert.Rune();
                            rune.x = x;
                            rune.y = y + Wert.CurrentStage.LastLine;
                            Wert.CurrentStage.waterfalls.addChild(rune);
                        }
                        d += 10 + Math.random() * 10;
                    }
                    d -= dist;
                }
            }
            break;
        default:
            console.log("Unknown type: " + meta.type);
            //DefaultBuilder(meta);
            break;
    }
};

Wert.StageSandbox.prototype.AddMapPart = function()
{
    var partName;
    if (this.LastMapPart == null)
    {
        partName = "MapStart";
    }
    else
    {
        partName = "Part" + Math.floor(1 + Math.random() * 4);
    }
	const map = PIXI.Loader.shared.resources[partName].data;
	const atlas = PIXI.Loader.shared.resources['Tileset'];
    DefaultBuilder = PIXI.tiled.LayerBuildersMap.objectgroup.__gen[0];
    PIXI.tiled.LayerBuildersMap.objectgroup.__gen[0] = this.MyBuilder;

    this.LastMapPartStaticObjects = [];
    var partHeight = map.height * map.tileheight;
    this.LastLine -= partHeight;
	this.LastMapPart = PIXI.tiled.CreateStage({'tileset.png': atlas.texture}, map);
    this.LastMapPart.y = this.LastLine;
    this.LastMapPart.Bodies = this.LastMapPartStaticObjects;
    this.MapParts.push(this.LastMapPart);
    this.foreground.addChild(this.LastMapPart);

}

Wert.StageSandbox.prototype.UpdateHearts = function()
{
    for (var i = 0; i < this.hero._maxHealth; i++)
    {
        var hearth = this.heartsContainer.children[i];
        hearth.visible = i < this.hero._health;    
    }
}

Wert.StageSandbox.prototype.EndLevel = function()
{
    this.State = StageState.EndLevel;
}