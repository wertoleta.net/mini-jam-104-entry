PIXI.tiled.Inject(PIXI, { injectMiddleware: true, debugContainers: false, autoCreateStage: false });

var Wert = function(){
};

Wert.App = null;
Wert.CurrentStage = null;

Wert.ViewportSize = {
	max: {
		width: 1920,
		height: 1080
	},
	min: {
		width: 800,
		height: 544
	},
	current: {
		width: 0,
		height: 0
	}
};

Wert.ViewportPosition= {
	screen: {
		x: 0,
		y: 0
	},
	stage: {
		x: 0,
		y: 0
	}
};

Wert.Center = {
	x: Wert.ViewportSize.max.width / 2,
	y: Wert.ViewportSize.max.height / 2
};

Wert.SwitchStage = function(newStage) {
	if (Wert.CurrentStage != null) {
		Wert.App.stage.removeChild(Wert.CurrentStage);
		Wert.CurrentStage = null;
	}
	Wert.CurrentStage = newStage;
	newStage.Create();
	newStage.Resize(Wert.ViewportSize.min);
	Wert.App.stage.addChild(Wert.CurrentStage);
}

Wert.Resize = function(newSize) {	
	var currentAspectRatio = newSize.width / newSize.height;
	var targetScale = 1.0;
	var targetAspectRatio = Wert.ViewportSize.min.width / Wert.ViewportSize.min.height;
	let canvas = document.querySelector('canvas');
	let w, h;
	if (currentAspectRatio > targetAspectRatio)
	{
		h = newSize.height;
		w = Math.floor(h * targetAspectRatio);
		
		targetScale = newSize.height / Wert.ViewportSize.current.height;
	}
	else
	{
		w = newSize.width;
		h =  Math.floor(w / targetAspectRatio);
		
		targetScale = newSize.width / Wert.ViewportSize.current.width;
	}
	canvas.style.height = h + 'px';
	canvas.style.width = w + 'px';
		
	//Wert.ViewportPosition.stage.x = Math.max(0, (Wert.ViewportSize.max.width - Wert.ViewportSize.current.width) / 2);
	//Wert.ViewportPosition.stage.y = Math.max(0, (Wert.ViewportSize.max.height - Wert.ViewportSize.current.height) / 2);
	
	//Wert.App.stage.scale.x = targetScale;
	//Wert.App.stage.scale.y = targetScale;

	canvas.style.left = Math.floor(( newSize.width - w) / 2) + 'px';
	canvas.style.top = Math.floor(( newSize.height - h) / 2) + 'px';
	//Wert.App.stage.x = Math.floor(newSize.width / 2 - targetScale * Wert.ViewportSize.max.width / 2);
	//Wert.App.stage.y = Math.floor(newSize.height / 2 - targetScale * Wert.ViewportSize.max.height / 2);
	
	//Wert.ViewportPosition.screen.x = Math.max(0, Wert.App.stage.x);
	//Wert.ViewportPosition.screen.y = Math.max(0, Wert.App.stage.y);
	
	//Wert.App.renderer.resize(newSize.width, newSize.height);
	//if (Wert.CurrentStage != null) {
	//	Wert.CurrentStage.Resize(Wert.ViewportSize.current);
	//}
}

Wert.Resize2 = function(newSize) {
	newSize = Wert.ViewportSize.min;
	
	var currentAspectRatio = newSize.width / newSize.height;
	var targetScale = 1.0;
	var targetAspectRatio = Wert.ViewportSize.min.width / Wert.ViewportSize.min.height;
	if (currentAspectRatio > targetAspectRatio)
	{
		Wert.ViewportSize.current.height = Wert.ViewportSize.min.height;
		Wert.ViewportSize.current.width = Wert.ViewportSize.current.height * currentAspectRatio;
		
		targetScale = newSize.height / Wert.ViewportSize.current.height;
	}
	else
	{
		Wert.ViewportSize.current.width = Wert.ViewportSize.min.width;
		Wert.ViewportSize.current.height =  Wert.ViewportSize.current.width / currentAspectRatio;
		
		targetScale = newSize.width / Wert.ViewportSize.current.width;
	}
		
	Wert.ViewportPosition.stage.x = Math.max(0, (Wert.ViewportSize.max.width - Wert.ViewportSize.current.width) / 2);
	Wert.ViewportPosition.stage.y = Math.max(0, (Wert.ViewportSize.max.height - Wert.ViewportSize.current.height) / 2);
	
	Wert.App.stage.scale.x = targetScale;
	Wert.App.stage.scale.y = targetScale;

	//Wert.App.stage.x = Math.floor(newSize.width / 2 - targetScale * Wert.ViewportSize.max.width / 2);
	//Wert.App.stage.y = Math.floor(newSize.height / 2 - targetScale * Wert.ViewportSize.max.height / 2);
	
	Wert.ViewportPosition.screen.x = Math.max(0, Wert.App.stage.x);
	Wert.ViewportPosition.screen.y = Math.max(0, Wert.App.stage.y);
	
	Wert.App.renderer.resize(newSize.width, newSize.height);
	if (Wert.CurrentStage != null) {
		Wert.CurrentStage.Resize(Wert.ViewportSize.current);
	}
}

Wert.Update = function(time) {
	Wert._upAction.Update();
	Wert._leftAction.Update();
	Wert._downAction.Update();
	Wert._rightAction.Update();
	Wert._attackAction.Update();
	if (Wert.CurrentStage != null) {
		Wert.CurrentStage.Update();
	}
};

Wert.GlobalToViewport = function(global) {
	return {
		x: (global.x - Wert.ViewportPosition.screen.x) / Wert.App.stage.scale.x + Wert.ViewportPosition.stage.x,
		y: (global.y - Wert.ViewportPosition.screen.y) / Wert.App.stage.scale.y + Wert.ViewportPosition.stage.y
	};
};

function Extend(Child, Parent) {
	Child.prototype = Object.create(Parent.prototype)
    Child.prototype.constructor = Child;
    Child.super = Parent.prototype;
}

PIXI.DisplayObject.prototype.Update = function()
{
}

PIXI.Container.prototype.Update = function()
{
	for (var childIndex = 0; childIndex < this.children.length; childIndex++)
	{
		this.getChildAt(childIndex).Update();
	}
} 

Wert.LineIntersection = function(l1, l2)
{
    let s1_x, s1_y, s2_x, s2_y;
    s1_x = l1.p2.x - l1.p1.x; 
    s1_y = l1.p2.y - l1.p1.y;
	s2_x = l2.p2.x - l2.p1.x;     
	s2_y = l2.p2.y - l2.p1.y;
	
    let s, t;
    s = (-s1_y * (l1.p1.x - l2.p1.x) + s1_x * (l1.p1.y - l2.p1.y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (l1.p1.y - l2.p1.y) - s2_y * (l1.p1.x - l2.p1.x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        return {
			x: l1.p1.x + (t * s1_x),
			y: l1.p1.y + (t * s1_y)
		};
    }

    return null;
}

Wert.SoundPlayed = false;
Wert.IsMuted = true;

var IsKeyPressed = [];

Wert.Debug = false;
document.addEventListener("keydown", (event)=>{
	if (IsKeyPressed[event.key] === true)
	{
		return;
	}
	IsKeyPressed[event.key] = true;
	switch(event.key)
	{
		case "w":
		case "ArrowUp":
		case " ":
			Wert._upAction.Activate();
			break;
		case "a":
		case "ArrowLeft":
			Wert._leftAction.Activate();
			break;
		case "s":
		case "ArrowDown":
			Wert._downAction.Activate();
			break;
		case "d":
		case "ArrowRight":
			Wert._rightAction.Activate();
			break;
		case "Control":
		case "x":
		case "n":
			Wert._attackAction.Activate();
			break;
		case "q":
			Wert.Debug = !Wert.Debug;
			break;
	}
}, false);

document.addEventListener("keyup", (event)=>{
	IsKeyPressed[event.key] = false;
	switch(event.key)
	{
		case "w":
		case "ArrowUp":
		case " ":
			Wert._upAction.Deactivate();
			break;
		case "a":
		case "ArrowLeft":
			Wert._leftAction.Deactivate();
			break;
		case "s":
		case "ArrowDown":
			Wert._downAction.Deactivate();
			break;
		case "d":
		case "ArrowRight":
			Wert._rightAction.Deactivate();
			break;
		case "Control":
		case "x":
		case "n":
			Wert._attackAction.Deactivate();
			break;
	}
}, false);