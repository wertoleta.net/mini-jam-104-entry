Wert.Corners = function(left, top, right, bottom)
{
    this.Left = left;
    this.Top = top;
    this.Right = right;
    this.Bottom = bottom;
};

Wert.Corners.prototype.Left = 0;
Wert.Corners.prototype.Right = 0;
Wert.Corners.prototype.Top = 0;
Wert.Corners.prototype.Bottom = 0;
        
Wert.Corners.prototype.IsBetweenTopAndBottom = function(yCoordinate)
{
    return (yCoordinate >= this.Top && yCoordinate <= this.Bottom);
}

Wert.Corners.prototype.IsBetweenLeftAndRight = function(xCoordinate)
{
    return (xCoordinate >= this.Left && xCoordinate <= this.Right);
}

Wert.Corners.prototype.CopyFrom = function(other)
{
    this.Left = other.Left;
    this.Right = other.Right;
    this.Top = other.Top;
    this.Bottom = other.Bottom;
}

Wert.Side = function()
{
}

Wert.Side.None			    = 0b0000; //0
Wert.Side.Left			    = 0b0001; //1
Wert.Side.Top				= 0b0010; //2
Wert.Side.LeftTop			= 0b0011; //3
Wert.Side.Right			    = 0b0100; //4
Wert.Side.Horizontal		= 0b0101; //5
Wert.Side.RightTop		    = 0b0110; //6
Wert.Side.HorizontalTop	    = 0b0111; //7
Wert.Side.Bottom			= 0b1000; //8
Wert.Side.BottomLeft		= 0b1001; //9
Wert.Side.Vertical		    = 0b1010; //10
Wert.Side.VerticalLeft	    = 0b1011; //11
Wert.Side.BottomRight		= 0b1100; //12
Wert.Side.HorizontalBottom  = 0b1101; //13
Wert.Side.VerticalRight	    = 0b1110; //14
Wert.Side.All				= 0b1111; //15

Wert.RectangleBody = function(rect, type) {
	Wert.RectangleBody.super.constructor.call(this, type, rect.Position);
    this._size = rect.Size;
    this._corners = new Wert.Corners();
    this._previousCorners = new Wert.Corners();
    this._solidSides = Wert.Side.All;
    this._collidedSides = Wert.Side.None;
    this._touchedSides = Wert.Side.None;
    this._touchedBodies = [];
    this._touchedSidesKeys.forEach(side => {
        this._touchedBodies[side] = [];
    });
    this.UpdateCorners();

    this.prevWH = 0;
    this.myRect = new PIXI.Graphics();
    this.addChild(this.myRect);
}

Extend(Wert.RectangleBody, Wert.PrimitiveBody);

Wert.RectangleBody.prototype._size = null;
Wert.RectangleBody.prototype._corners = null;
Wert.RectangleBody.prototype._previousCorners = null;
Wert.RectangleBody.prototype._solidSides = null;
Wert.RectangleBody.prototype._collidedSides = null;
Wert.RectangleBody.prototype._touchedSides = null;
Wert.RectangleBody.prototype._touchedBodies = null;
Wert.RectangleBody.prototype._collidedBodies = null;
Wert.RectangleBody.prototype._touchedSidesKeys = [
    Wert.Side.Left,
    Wert.Side.Top,
    Wert.Side.Right,
    Wert.Side.Bottom
];

Wert.RectangleBody.prototype.Update = function() {
	Wert.RectangleBody.super.Update.call(this);

    this.myRect.clear();
    if (Wert.Debug)
    {
        this.myRect.lineStyle(1, 0xFFFFFF);
        this.myRect.drawRect(0, 0, this.GetSize().Width, this.GetSize().Height); // x, y, width, height
    }

    this.x = this._position._current.X;
    this.y = this._position._current.Y;
}

Wert.RectangleBody.prototype.IsOverlaps = function(anotherRectangle)
{
    var anotherCorners = anotherRectangle.GetCorners();
    var corners = this.GetCorners();
    const eps = 0.999;
    return	((anotherCorners.Right + eps) >= corners.Left) &&
        ((anotherCorners.Left - eps) <= corners.Right) &&
        ((anotherCorners.Bottom + eps) >= corners.Top) &&
        ((anotherCorners.Top - eps) <= corners.Bottom);
}

Wert.RectangleBody.prototype.IsTouches = function(anotherRectangle)
{
    var anotherCorners = anotherRectangle.GetCorners();
    var corners = this.GetCorners();
    const eps = 1.999;
    return	((anotherCorners.Right + eps) >= corners.Left) &&
        ((anotherCorners.Left - eps) <= corners.Right) &&
        ((anotherCorners.Bottom + eps) >= corners.Top) &&
        ((anotherCorners.Top - eps) <= corners.Bottom);
}

Wert.RectangleBody.prototype.GetOverlapSize = function(anotherRectangle)
{
    var anotherCorners = anotherRectangle.GetCorners();
    var corners = this.GetCorners();

    var xOverlap;
    if (corners.Left > anotherCorners.Left)
    {
        xOverlap = anotherCorners.Right - corners.Left + 1;
    }
    else
    {
        xOverlap = corners.Right - anotherCorners.Left + 1;
    }

    var yOverlap;
    if (corners.Top > anotherCorners.Top)
    {
        yOverlap = anotherCorners.Bottom - corners.Top + 1;
    }
    else
    {
        yOverlap = corners.Bottom - anotherCorners.Top + 1;
    }

    return new Wert.FloatSize(Math.max(0, xOverlap), Math.max(0, yOverlap));
}


Wert.RectangleBody.prototype.GetSize = function()
{
    return this._size;
}

Wert.RectangleBody.prototype.SetSize = function(newSize)
{
    this._size = newSize;
    this.UpdateCorners();
}

Wert.RectangleBody.prototype.GetCorners = function()
{
    return this._corners;
}

Wert.RectangleBody.prototype.GetPreviousCorners = function()
{
    return this._previousCorners;
}

Wert.RectangleBody.prototype.UpdateCorners = function()
{
    var position = this._position.GetCurrent();
    this._corners.Left = position.X;
    this._corners.Top = position.Y;
    this._corners.Right = position.X + this._size.Width;
    this._corners.Bottom = position.Y + this._size.Height;
}

Wert.RectangleBody.prototype.MoveTo = function(x, y)
{
    Wert.RectangleBody.super.MoveTo.call(this, x, y);
    this.UpdateCorners();
}

Wert.RectangleBody.prototype.IsSideSolid = function(side)
{
    return (side & this._solidSides) == side;
}

Wert.RectangleBody.prototype.SetSolidSides = function(sides)
{
    this._solidSides = sides;
}

Wert.RectangleBody.prototype.StorePreviousCorners = function()
{
    this._previousCorners.CopyFrom(this._corners);
}

Wert.RectangleBody.prototype.ResetInteractions = function()
{
    this._collidedSides = Wert.Side.None;
    this._touchedSides = Wert.Side.None;
    this._touchedSidesKeys.forEach(side => {
        this._touchedBodies[side] = [];
    });
}

Wert.RectangleBody.prototype.IsSideCollided = function(side)
{
    return (this._collidedSides & side) == side;
}

Wert.RectangleBody.prototype.AddCollidedBody = function(side, body)
{
    this._collidedSides = this._collidedSides | side;
    this.AddTouchedBody(side, body);
}

Wert.RectangleBody.prototype.IsSideTouched = function(side)
{
    return (this._touchedSides & side) == side;
}

Wert.RectangleBody.prototype.GetTouchedBodies = function(side)
{
    var list = [];
    this._touchedSidesKeys.forEach(keySide => {
        if ((keySide & side) > 0)
        {
            var sideList = this._touchedBodies[keySide];
            for (var bodyIndex = 0; bodyIndex < sideList.length; bodyIndex++)
            {
                list.push(sideList[bodyIndex]);
            }
        }
    });
    return list;
}

Wert.RectangleBody.prototype.AddTouchedBody = function(side, body)
{
    this._touchedSides = this._touchedSides | side;
    this._touchedSidesKeys.forEach(keySide => {
        if ((keySide & side) > 0)
        {
            this._touchedBodies[keySide].push(body);
        }
    });
}