Wert.RectangleSpace = function() {
	Wert.RectangleSpace.super.constructor.call(this);

	this._bodies = [];
}

Extend(Wert.RectangleSpace, PIXI.Container);

Wert.RectangleSpace.prototype.GravityPower = 0.5;
Wert.RectangleSpace.prototype._bodies = null;

Wert.RectangleSpace.prototype.Update = function() {

	Wert.RectangleSpace.super.Update.call(this);

	var timeScale = 0.03;
	this.ResetBodiesStatuses(timeScale);
	for (var firstBodyIndex = 0; firstBodyIndex < this._bodies.length; firstBodyIndex++)
	{
		var firstBody = this._bodies[firstBodyIndex];
		if (firstBody.GetType() == Wert.MovableType.Dynamic)
		{
			for (var secondBodyIndex = 0; secondBodyIndex < this._bodies.length; secondBodyIndex++)
			{
				var secondBody = this._bodies[secondBodyIndex];
				var alreadyCalculated = /*secondBodyIndex < firstBodyIndex &&*/ secondBody.GetType() == Wert.MovableType.Dynamic;
				var sameBody = firstBodyIndex == secondBodyIndex;
				if (sameBody || alreadyCalculated)
				{
					continue;
				}
				if (firstBody.IsOverlaps(secondBody))
				{
					if (firstBody.IsSideSolid(Wert.Side.All) && secondBody.IsSideSolid(Wert.Side.All))
					{
						this.CalculateFullSolidCollisions(firstBody, secondBody);
					}
					else
					{
						this.CalculatePartitialSolidCollisions(firstBody, secondBody);
					}
				}
				else if (firstBody.IsTouches(secondBody))
				{
					this.CalculateTouches(firstBody, secondBody);						
				}
			}
		}
		firstBody.StorePreviousCorners();
	}
	this.ApplyGravity(timeScale);
}

Wert.RectangleSpace.prototype.GetAllOverlaped = function(body)
{
	var list = [];
	for (var secondBodyIndex = 0; secondBodyIndex < this._bodies.length; secondBodyIndex++)
	{
		var secondBody = this._bodies[secondBodyIndex];
		var sameBody = body == secondBody;
		if (sameBody)
		{
			continue;
		}
		if (body.IsOverlaps(secondBody))
		{
			list.push(secondBody);
		}
	}
	return list;
}

Wert.RectangleSpace.prototype.ResetBodiesStatuses = function()
{
	for (var bodyIndex = 0; bodyIndex < this._bodies.length; bodyIndex++)
	{
		var body = this._bodies[bodyIndex];
		body.ResetInteractions();
		if (body.GetType() == Wert.MovableType.Dynamic)
		{
			body.ApplyVelocity(1.0);
		}
	}
}

Wert.RectangleSpace.prototype.ApplyGravity = function(timeScale)
{
	for (var bodyIndex = 0; bodyIndex < this._bodies.length; bodyIndex++)
	{
		var body = this._bodies[bodyIndex];
		if (body.GetType() == Wert.MovableType.Dynamic)
		{
			body.AddVelocity(0.0, this.GravityPower);
		}
	}
}

Wert.RectangleSpace.prototype.CalculateFullSolidCollisions = function(firstBody, secondBody)
{
	var massCoefficient1 = 0.5;
	var massCoefficient2 = 0.5;
	if (secondBody.GetType() == Wert.MovableType.Dynamic)
	{
		var totalWeight = Math.max(firstBody.GetWeight() + secondBody.GetWeight(), 1.0);
		massCoefficient1 = firstBody.GetWeight() / totalWeight;
	}
	else
	{
		massCoefficient1 = 1.0;
	}
	massCoefficient2 = 1.0 - massCoefficient1;
	var overlapSize = firstBody.GetOverlapSize(secondBody);
	var velocity = firstBody.GetVelocity();
	if (overlapSize.Width <= overlapSize.Height)
	{
		if (firstBody.GetPreviousPosition().X < secondBody.GetPreviousPosition().X)
		{
			massCoefficient1 = -massCoefficient1;
			firstBody.AddCollidedBody(Wert.Side.Right, secondBody);
			secondBody.AddCollidedBody(Wert.Side.Left, firstBody);
		}
		else
		{
			massCoefficient2 = -massCoefficient2;
			firstBody.AddCollidedBody(Wert.Side.Left, secondBody);
			secondBody.AddCollidedBody(Wert.Side.Right, firstBody);
		}
		firstBody.MoveOn(overlapSize.Width * massCoefficient1, 0.0);
		secondBody.MoveOn(overlapSize.Width * massCoefficient2, 0.0);

		if ((velocity.X > 0.0 && massCoefficient1 < 0.0) ||
			(velocity.X < 0.0 && massCoefficient1 > 0.0))
		{
			firstBody.SetVelocity(0.0, velocity.Y);
		}
	}
	else
	{
		if (firstBody.GetPreviousPosition().Y < secondBody.GetPreviousPosition().Y)
		{
			massCoefficient1 = -massCoefficient1;
			firstBody.AddCollidedBody(Wert.Side.Bottom, secondBody);
			secondBody.AddCollidedBody(Wert.Side.Top, firstBody);
		}
		else
		{
			massCoefficient2 = -massCoefficient2;
			firstBody.AddCollidedBody(Wert.Side.Top, secondBody);
			secondBody.AddCollidedBody(Wert.Side.Bottom, firstBody);
		}
		firstBody.MoveOn(0.0, overlapSize.Height * massCoefficient1);
		secondBody.MoveOn(0.0, overlapSize.Height * massCoefficient2);

		if ((velocity.Y > 0.0 && massCoefficient1 < 0.0) ||
			(velocity.Y < 0.0 && massCoefficient1 > 0.0))
		{
			firstBody.SetVelocity(velocity.X, 0.0);
		}
	}
}

Wert.RectangleSpace.prototype.CalculatePartitialSolidCollisions = function(firstBody, secondBody)
{
	var massCoefficient1 = this.GetMassCoefficient(firstBody, secondBody);
	var massCoefficient2 = 1.0 - massCoefficient1;
	var overlapSize = firstBody.GetOverlapSize(secondBody);
	var velocity = firstBody.GetVelocity();
	var collidedSides = Wert.Side.None;

	if (firstBody.IsSideSolid(Wert.Side.Bottom) && secondBody.IsSideSolid(Wert.Side.Top) && firstBody.GetPreviousCorners().Bottom < secondBody.GetPreviousCorners().Top)
	{
		collidedSides = collidedSides | Wert.Side.Bottom;
	}
	if (firstBody.IsSideSolid(Wert.Side.Top) && secondBody.IsSideSolid(Wert.Side.Bottom) && firstBody.GetPreviousCorners().Top > secondBody.GetPreviousCorners().Bottom)
	{
		collidedSides = collidedSides | Wert.Side.Top;
	}
	if (firstBody.IsSideSolid(Wert.Side.Right) && secondBody.IsSideSolid(Wert.Side.Left) && firstBody.GetPreviousCorners().Right < secondBody.GetPreviousCorners().Left)
	{
		collidedSides = collidedSides | Wert.Side.Right;
	}
	if (firstBody.IsSideSolid(Wert.Side.Left) && secondBody.IsSideSolid(Wert.Side.Right) && firstBody.GetPreviousCorners().Left > secondBody.GetPreviousCorners().Right)
	{
		collidedSides = collidedSides | Wert.Side.Left;
	}
	if ((collidedSides & Wert.Side.Horizontal) != Wert.Side.None && (collidedSides & Wert.Side.Vertical) != Wert.Side.None)
	{
		if (overlapSize.Width <= overlapSize.Height)
		{
			collidedSides = collidedSides & Wert.Side.Horizontal;
		}
		else
		{
			collidedSides = collidedSides & Wert.Side.Vertical;
		}
	}
	switch (collidedSides)
	{
	case Wert.Side.Bottom:
		firstBody.MoveOn(0.0, -overlapSize.Height * massCoefficient1);
		secondBody.MoveOn(0.0, overlapSize.Height * massCoefficient2);
		firstBody.AddCollidedBody(Wert.Side.Bottom, secondBody);
		secondBody.AddCollidedBody(Wert.Side.Top, firstBody);
		if (velocity.Y > 0.0)
		{
			firstBody.SetVelocity(velocity.X, 0.0);
		}
		break;
	case Wert.Side.Top:
		firstBody.MoveOn(0.0, overlapSize.Height * massCoefficient1);
		secondBody.MoveOn(0.0, -overlapSize.Height * massCoefficient2);
		firstBody.AddCollidedBody(Wert.Side.Top, secondBody);
		secondBody.AddCollidedBody(Wert.Side.Bottom, firstBody);
		if (velocity.Y < 0.0)
		{
			firstBody.SetVelocity(velocity.X, 0.0);
		}
		break;
	case Wert.Side.Right:
		firstBody.MoveOn(-overlapSize.Width * massCoefficient1, 0.0);
		secondBody.MoveOn(overlapSize.Width * massCoefficient2, 0.0);
		firstBody.AddCollidedBody(Wert.Side.Right, secondBody);
		secondBody.AddCollidedBody(Wert.Side.Left, firstBody);
		if (velocity.X > 0.0)
		{
			firstBody.SetVelocity(0.0, velocity.Y);
		}
		break;
	case Wert.Side.Left:
		firstBody.MoveOn(overlapSize.Width * massCoefficient1, 0.0);
		secondBody.MoveOn(-overlapSize.Width * massCoefficient2, 0.0);
		firstBody.AddCollidedBody(Wert.Side.Left, secondBody);
		secondBody.AddCollidedBody(Wert.Side.Right, firstBody);
		if (velocity.X < 0.0)
		{
			firstBody.SetVelocity(0.0, velocity.Y);
		}
		break;
	}
}

Wert.RectangleSpace.prototype.CalculateTouches = function(firstBody, secondBody)
{
	var firstCorners = firstBody.GetCorners();
	var secondCorners = secondBody.GetCorners();
	if (firstCorners.Top > secondCorners.Bottom)
	{
		firstBody.AddTouchedBody(Wert.Side.Top, secondBody);
		secondBody.AddTouchedBody(Wert.Side.Bottom, firstBody);
	}
	else if (firstCorners.Bottom < secondCorners.Top)
	{
		firstBody.AddTouchedBody(Wert.Side.Bottom, secondBody);
		secondBody.AddTouchedBody(Wert.Side.Top, firstBody);
	}
	if (firstCorners.Left > secondCorners.Right)
	{
		firstBody.AddTouchedBody(Wert.Side.Left, secondBody);
		secondBody.AddTouchedBody(Wert.Side.Right, firstBody);
	}
	else if (firstCorners.Right < secondCorners.Left)
	{
		firstBody.AddTouchedBody(Wert.Side.Right, secondBody);
		secondBody.AddTouchedBody(Wert.Side.Left, firstBody);
	}
}

Wert.RectangleSpace.prototype.GetMassCoefficient = function(firstBody, secondBody)
{
	if (secondBody.GetType() == Wert.MovableType.Dynamic)
	{
		var totalWeight = Math.max(firstBody.GetWeight() + secondBody.GetWeight(), 1.0);
		return firstBody.GetWeight() / totalWeight;
	}
	else
	{
		return 1.0;
	}
}

Wert.RectangleSpace.prototype.AddBody = function(body)
{
	this._bodies.push(body);
	this.addChild(body);
}

Wert.RectangleSpace.prototype.RemoveBody = function(body)
{
	for (var i = 0; i < this._bodies.length; i++)
	{
		if (this._bodies[i] == body)
		{
			this._bodies.splice(i, 1);
			break;
		}
	}
	this.removeChild(body);
}