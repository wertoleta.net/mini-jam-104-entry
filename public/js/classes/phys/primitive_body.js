Wert.MovableType = function(){};

Wert.MovableType.Static = 0;
Wert.MovableType.Dynamic = 1;
Wert.MovableType.Kinetic = 2;

Wert.FloatPoint = function(x, y)
{
    this.X = x;
    this.Y = y;
}

Wert.FloatPoint.prototype.X = 0;
Wert.FloatPoint.prototype.Y = 0;

Wert.FloatSize = function(width, height)
{
    this.Width = width;
    this.Height = height;
}

Wert.FloatPoint.prototype.Width = 0;
Wert.FloatPoint.prototype.Height = 0;

Wert.Position = function(x, y){
    this._current = new Wert.FloatPoint();
    this._current.X = x;
    this._current.Y = y;
    this._previous = new Wert.FloatPoint();
    this._previous.X = x;
    this._previous.Y = y;
};

Wert.Position.prototype._current = null;
Wert.Position.prototype._previous = null;

Wert.Position.prototype.MoveOn = function(additionalX, additionalY)
{
    MoveTo(this._current.X + additionalX, this._current.Y + additionalY);
}

Wert.Position.prototype.MoveTo = function(newX, newY)
{
    this._previous = this._current;
    this._current = new Wert.FloatPoint(newX, newY);
}

Wert.Position.prototype.MoveToPosition = function(newPosition)
{
    this.MoveTo(newPosition.X, newPosition.Y);
}

Wert.Position.prototype.GetPrevious = function()
{
    return this._previous;
}

Wert.Position.prototype.GetCurrent = function()
{
    return this._current;
}

Wert.PrimitiveBody = function(type, spawnPoint)
{
	Wert.PrimitiveBody.super.constructor.call(this);

    this._type = type;
    this._position = new Wert.Position(spawnPoint.X, spawnPoint.Y);
    this._weight = 0;
    this._velocity = new Wert.FloatPoint(0.0, 0.0);
    this._maxVelocity = new Wert.FloatPoint(10, 10);
}

Extend(Wert.PrimitiveBody, PIXI.Container);

Wert.PrimitiveBody.prototype._type = Wert.MovableType.Static;
Wert.PrimitiveBody.prototype._weight = 0;
Wert.PrimitiveBody.prototype._velocity = null;
Wert.PrimitiveBody.prototype._maxVelocity = null;
Wert.PrimitiveBody.prototype._position = null;

Wert.PrimitiveBody.prototype.GetType = function()
{
    return this._type;
}

Wert.PrimitiveBody.prototype.GetWeight = function()
{
    return this._weight;
}

Wert.PrimitiveBody.prototype.GetCurrentPosition = function()
{
    return this._position.GetCurrent();
}

Wert.PrimitiveBody.prototype.GetPreviousPosition = function()
{
    return this._position.GetPrevious();
}

Wert.PrimitiveBody.prototype.MoveTo = function(x, y)
{
    this._position.MoveTo(x, y);
}

Wert.PrimitiveBody.prototype.MoveToPosition = function(newPosition)
{
    this._position.MoveTo(newPosition.X, newPosition.Y);
}

Wert.PrimitiveBody.prototype.MoveOnShift = function(additionalShift)
{
    this.MoveOn(additionalShift.X, additionalShift.Y);
}

Wert.PrimitiveBody.prototype.MoveOn = function(additionalX, additionalY)
{
    current = this._position.GetCurrent();
    this.MoveTo(current.X + additionalX, current.Y + additionalY);
}

Wert.PrimitiveBody.prototype.IsCollide = function(anotherBody)
{ 
    return false; 
};

Wert.PrimitiveBody.prototype.GetVelocity = function()
{
    return this._velocity;
}

Wert.PrimitiveBody.prototype.SetVelocity = function(x, y)
{
    this._velocity.X = Math.min(Math.abs(x), this._maxVelocity.X) * Math.sign(x);
    this._velocity.Y = Math.min(Math.abs(y), this._maxVelocity.Y) * Math.sign(y);
}

Wert.PrimitiveBody.prototype.AddVelocity = function(x, y)
{
    this.SetVelocity(this._velocity.X + x, this._velocity.Y + y);
}

Wert.PrimitiveBody.prototype.GetMaxVelocity = function()
{
    return this._maxVelocity;
}

Wert.PrimitiveBody.prototype.SetMaxVelocity = function(x, y)
{
    this._maxVelocity.X = Math.abs(x);
    this._maxVelocity.Y = Math.abs(y);
}

Wert.PrimitiveBody.prototype.ApplyVelocity = function(timeScale)
{
    this.MoveOn(this._velocity.X * timeScale, this._velocity.Y * timeScale);
}