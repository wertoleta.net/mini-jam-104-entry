Wert.BubbleMom = function() {
	Wert.BubbleMom.super.constructor.call(this);
	
	this.face = new PIXI.Sprite(this.SmileFaceTexture);
	this.face.anchor.set(0.5);
	
	this.eyelids = new PIXI.Sprite(this.EyelidsTexture);
	this.eyelids.visible = false;
	this.eyelids.anchor.set(0.5);
	
	this.eyelidsTimer = 0;
	this.shootCooldown = 0;
	this.state = Wert.BubbleHero.prototype.EState.Idle;
	this.upSpeed = 0;
	this.canHandleItem = false;
	this.isEmergeable = false;
	this.upSpeed = -1;
	
	this.SetArea(this.MediumSizeArea);
	
	this.addChild(this.face);
	this.addChild(this.eyelids);
};

Extend(Wert.BubbleMom, Wert.Bubble);

Wert.BubbleMom.prototype.SmileFaceTexture = null;
Wert.BubbleMom.prototype.SurprisedFaceTexture = null;
Wert.BubbleMom.prototype.EyelidsTexture = null;

Wert.BubbleMom.prototype.eyelids = null;
Wert.BubbleMom.prototype.eyelidsTimer = 0;
Wert.BubbleMom.prototype.face = null;

Wert.BubbleMom.prototype.Update = function(){
	Wert.BubbleMom.super.Update.call(this);
	
	if (this.speed.length() > 1) 
	{
		this.face.texture = this.SurprisedFaceTexture;
	}
	else 
	{
		this.face.texture = this.SmileFaceTexture;
	}
	
	this.eyelidsTimer--;
	if (this.eyelidsTimer <= 0) 
	{
		this.eyelidsTimer = this.eyelids.visible ? 60 + Math.floor(Math.random() * 30) : 10;
		this.eyelids.visible = !this.eyelids.visible;
	}
}