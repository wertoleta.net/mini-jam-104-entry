Wert.Stage = function() {
	Wert.Stage.super.constructor.call(this);
	this.mouse = {x: 0, y: 0, global: {x: 0, y: 0}, isDown: false, isPressed: false};
	
	this.interactive = true;
	
	this.on('mousemove', this.OnMove);
	this.on('touchmove', this.OnMove);
	this.on('mousedown', this.OnDown);
	this.on('touchstart', this.OnDown);
	this.on('mouseup', this.OnUp);
	this.on('touchend', this.OnUp);
}

Extend(Wert.Stage, PIXI.Container);

Wert.Stage.prototype.mouse = null;

Wert.Stage.prototype.Create = function() {	
	var max = Wert.ViewportSize.max;
	var min = Wert.ViewportSize.min;
};

Wert.Stage.prototype.Update = function() {
	Wert.Stage.super.Update.call(this);
	let viewport = this.WindowToStage(this.mouse.global);
	this.mouse.x = viewport.x;
	this.mouse.y = viewport.y;
	this.mouse.isPressed = false;
};
	
Wert.Stage.prototype.Resize = function(newSize) {
};

Wert.Stage.prototype.WindowToStage = function(global) {
	return {
		x: -this.x + global.x / Wert.App.stage.scale.x,
		y: -this.y + global.y / Wert.App.stage.scale.y
	};
	
};

Wert.Stage.prototype.OnDown = function(args) {	
	let viewport = this.WindowToStage(args.data.global);
	this.mouse.global = args.data.global;
	this.mouse.x = viewport.x; 
	this.mouse.y = viewport.y; 
	this.mouse.isDown = true;
	this.mouse.isPressed = true;
};

Wert.Stage.prototype.OnUp = function(args) {	
};

Wert.Stage.prototype.OnMove = function(args) {	
	let viewport = this.WindowToStage(args.data.global);
	this.mouse.global = args.data.global;
	this.mouse.x = viewport.x;
	this.mouse.y = viewport.y;
	this.mouse.isDown = false;
};