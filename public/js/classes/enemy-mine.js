Wert.EnemyMine = function() {
	Wert.EnemyMine.super.constructor.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	this.sprite = new PIXI.Sprite(sheet.textures['mine-big']);
	this.sprite.anchor.set(0.5);
	this.scale.set(1);
	this.addChild(this.sprite);
	
	this.radius = 16;
}

Extend(Wert.EnemyMine, Wert.Enemy);

Wert.EnemyMine.prototype.sprite = null;

Wert.EnemyMine.prototype.Update = function() {
	Wert.EnemyMine.super.Update.call(this);
	
	let hero = Wert.CurrentStage.hero;
	let direction = new Victor(this.position.x - hero.x, this.position.y - hero.y);
				
	if (direction.length() < hero.radius + this.radius)
	{
		hero.Kill();
	}
};

Wert.EnemyMine.prototype.CatchInBubble = function(bubble)
{
	this.CollideWithBubble(bubble);
}

Wert.EnemyMine.prototype.CollideWithBubble = function(bubble)
{
	Wert.CurrentStage.bubbles.splice(Wert.CurrentStage.bubbles.indexOf(bubble), 1);
	Wert.CurrentStage.removeChild(bubble);
	
	if (!Wert.IsMuted)
	{
		PIXI.Loader.shared.resources["Pop"].data.currentTime = 0;
		PIXI.Loader.shared.resources["Pop"].data.play();
	}
	
	if (bubble.item != null)
	{
		bubble.item.FreeFromBubble();
	}
}