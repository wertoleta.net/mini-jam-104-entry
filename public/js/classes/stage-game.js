Wert.StageGame = function(levelNumber) {
	Wert.StageGame.super.constructor.call(this);
	
	if (levelNumber > 0)
	{
		this.levelNumber = levelNumber;
		this.map = PIXI.Loader.shared.resources["Level" + levelNumber].data;
		this.map.size = {
			width: Math.max(this.map.width * this.map.tilewidth, Wert.ViewportSize.min.width),
			height: Math.max(this.map.height * this.map.tileheight, Wert.ViewportSize.min.height),
		};
	}
	this.innerLayer = new PIXI.Container();
	this.textLayer = new PIXI.Container();
}

Extend(Wert.StageGame, Wert.Stage);

Wert.StageGame.prototype.levelNumber = 0;
Wert.StageGame.prototype.map = null;
Wert.StageGame.prototype.buttonOffset = 8;
Wert.StageGame.prototype.innerLayer = null;
Wert.StageGame.prototype.textLayer = null;
Wert.StageGame.prototype.restartButton = null;
Wert.StageGame.prototype.homeButton = null;
Wert.StageGame.prototype.soundButton = null;


Wert.StageGame.prototype.Create = function() {
	Wert.StageGame.super.Create.call(this);
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	
	this.restartButton = new Wert.Button(sheet.textures['buttons/restart'], function(){
		Wert.SwitchStage(new Wert.StageGame(Wert.CurrentStage.levelNumber))
	});
	this.restartButton.normalScale = 0.5;
	
	this.homeButton = new Wert.Button(sheet.textures['buttons/home'], function(){
		Wert.SwitchStage(new Wert.StageMainMenu())
	});
	this.homeButton.normalScale = 0.5;
	
	this.soundButton = new Wert.Button(sheet.textures[Wert.IsMuted ? 'buttons/sound-off' : 'buttons/sound-on'], function(){
		if (!Wert.SoundPlayed)
		{
			PIXI.Loader.shared.resources["BackgroundMusic"].data.play();
			Wert.SoundPlayed = true;
			Wert.IsMuted = false;
		}
		else
		{
			Wert.IsMuted = !Wert.IsMuted;
			PIXI.Loader.shared.resources["BackgroundMusic"].data.muted = Wert.IsMuted;
		}
		this.sprite.texture = sheet.textures[Wert.IsMuted ? 'buttons/sound-off' : 'buttons/sound-on'];
	});
	this.soundButton.normalScale = 0.5;
	
	this.addChild(this.innerLayer);	
	
	this.FillStage(this.map);
	
	this.addChild(this.textLayer);	
	this.addChild(this.restartButton);	
	this.addChild(this.homeButton);	
	this.addChild(this.soundButton);	
};

Wert.StageGame.prototype.Update = function() {
	Wert.StageGame.super.Update.call(this);
	
	this.restartButton.x = -this.x + this.restartButton.width / 2 + 8;
	this.restartButton.y = -this.y + this.restartButton.height / 2 + 8;
	
	this.homeButton.x = -this.x + Wert.ViewportSize.min.width - this.homeButton.width * 1.3 - 8;
	this.homeButton.y = -this.y + this.homeButton.height / 2 + 8;
	
	this.soundButton.x = -this.x + Wert.ViewportSize.min.width - this.soundButton.width / 2 - 8;
	this.soundButton.y = -this.y + this.soundButton.height * 1.3 + 8;
};

Wert.StageGame.prototype.Resize = function(newSize) {	
	Wert.StageGame.super.Resize.call(this, newSize);
};

Wert.StageGame.prototype.OnDown = function(args) {	
	Wert.StageGame.super.OnDown.call(this, args);
	let x = this.mouse.x;
	let y = this.mouse.y;
};

Wert.StageGame.prototype.OnUp = function(args) {	
	Wert.StageGame.super.OnUp.call(this, args);
};

Wert.StageGame.prototype.OnMove = function(args) {	
	Wert.StageGame.super.OnMove.call(this, args);
};

Wert.StageGame.prototype.FillStage = function(map)
{
	let terrainLayer = null;
	let collisionLayer = null;
	let objectsLayer = null;
	let imagesLayer = null;
	
	let sheet = PIXI.Loader.shared.resources["Atlas"].spritesheet;
	
	let layers = map.layers;
	for (let layerIndex = 0; layerIndex < layers.length; layerIndex++)
	{
		let layer = layers[layerIndex];
		switch(layer.name)
		{
			case "Terrain":
				terrainLayer = layer;
				break;
			case "Collisions":
				collisionLayer = layer;
				break;
			case "Objects":
				objectsLayer = layer;
				break;
			case "Tiles":
				imagesLayer = layer;
				break;
		}
	}
	if (objectsLayer != null)
	{
		let objects = objectsLayer.objects;
		for (let objectIndex = 0; objectIndex < objects.length; objectIndex++)
		{
			let object = objects[objectIndex];
			let stageObject = null;
			let layer = this;
			switch (object.type)
			{
				case "text":
					let text = object.text;
					stageObject = new PIXI.Text(text.text, {
						fontFamily : text.fontfamily ? text.fontfamily : 'Arial', 
						fontSize: text.pixelsize ? text.pixelsize : 16,
						fill: PIXI.utils.string2hex(text.color ? text.color : "#000000"),
						wordWrap: text.wrap,
						wordWrapWidth: object.width,
						align: text.halign ? text.halign : 'left'
					});
					stageObject.position.set(object.x, object.y);
					
					layer = this.textLayer;
					break;
			}
			if (stageObject != null)
			{
				layer.addChild(stageObject);
			}
		}		
	}
	if (imagesLayer != null)
	{
		let tiles = PIXI.Loader.shared.resources["Tileset"].data.tiles;
		let objects = imagesLayer.objects;
		for (let objectIndex = objects.length - 1; objectIndex >= 0; objectIndex--)
		{
			let object = objects[objectIndex];
			let textureName = null;
			for (let tileIndex = 0; tileIndex < tiles.length; tileIndex++)
			{
				let tile = tiles[tileIndex];
				if (object.gid - 1 === tile.id)
				{
					textureName = tile.image;
					textureName = textureName.substr(textureName.lastIndexOf('/') + 1);
					textureName = textureName.substr(0, textureName.lastIndexOf('.'));
					break;
				}
			}		
			
			let sprite = new PIXI.Sprite(sheet.textures['tiles/' + textureName]);
			sprite.position.set(object.x, object.y);
			sprite.anchor.set(0, 1);
			this.addChild(sprite);
		}
	}
};

Wert.StageGame.prototype.GotoNextLevel = function()
{
	let storedLevel = null;
	try {
		storedLevel = window.localStorage.getItem('avaiableLevel');
	}catch{}
	let nextLevelNumber = this.levelNumber + 1;
	let maxLevel = storedLevel ? Math.max(parseInt(storedLevel), nextLevelNumber) : nextLevelNumber;
	try
	{
		window.localStorage.setItem('avaiableLevel', maxLevel);
	}catch{
		Wert.StageLevelSelect.prototype.avaiableLevel = maxLevel;
	}
	Wert.SwitchStage(new Wert.StageGame(this.levelNumber + 1));
};