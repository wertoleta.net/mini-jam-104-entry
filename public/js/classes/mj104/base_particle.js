Wert.BaseParticle = function(){
    Wert.BaseParticle.super.constructor.call(this);
    this._velocity = {x: -8 + 16 * Math.random(), y: -4 - 4 * Math.random()};
    this.angle = Math.random() * 360;
    this._rotationDirection = Math.random() >= 0.5 ? 1 : -1;
};

Extend(Wert.BaseParticle, PIXI.Container);

Wert.BaseParticle.prototype._velocity = null;
Wert.BaseParticle.prototype._rotationDirection = 0;


Wert.BaseParticle.prototype.Update = function(){
    Wert.BaseParticle.super.Update.call(this);
    this.x += this._velocity.x;
    this.y += this._velocity.y;

    this._velocity.y += 0.25;
    
    if (Math.abs(this._velocity.x) < 0.01)
    {
        this._velocity.x = 0;
    }
    else
    {
        var step = Math.max(Math.abs(this._velocity.x) / 32.0, 0.01);
        this._velocity.x += -step * Math.sign(this._velocity.x);
    }

    if (this.y > Wert.CurrentStage.BottomLine)
    {
        this.parent.removeChild(this);
    }
    this.angle += this._rotationDirection * 4;
};