Wert.ActionState = {
    ASNone: 0,
    ASFired: 1,
    ASActive: 2,
    ASEnds: 3
};

Wert.Action = function(){
    this._state = Wert.ActionState.ASNone;
    this._nextState = Wert.ActionState.ASNone;
    this._activationsCount = 0;
};

Wert.Action.prototype.Update = function() 
{
    switch (this._state)
    {
    case Wert.ActionState.ASFired:
        this._state = Wert.ActionState.ASActive;
        break;
    case Wert.ActionState.ASEnds:
        this._state = Wert.ActionState.ASNone;
        break;
    }
    if (this._nextState != Wert.ActionState.ASNone)
    {
        this._state = this._nextState;
        this._nextState = Wert.ActionState.ASNone;
    }
}

Wert.Action.prototype.Activate = function()
{
    if (this._activationsCount == 0)
    {
        this._nextState = Wert.ActionState.ASFired;
    }
    this._activationsCount++;
}

Wert.Action.prototype.Deactivate = function() 
{
    this._activationsCount = Math.max(0, this._activationsCount - 1);
    if (this._activationsCount == 0)
    {
        this._nextState = Wert.ActionState.ASEnds;
    }
}

Wert.Action.prototype.IsFired = function()
{
    return this._state == Wert.ActionState.ASFired;
}

Wert.Action.prototype.IsActive = function()
{
    return this._state != Wert.ActionState.ASNone;
}

Wert.Action.prototype.IsEnds = function()
{
    return this._state == Wert.ActionState.ASEnds;
}
    
Wert._upAction = new Wert.Action();
Wert._leftAction = new Wert.Action();
Wert._rightAction = new Wert.Action();
Wert._downAction = new Wert.Action();
Wert._attackAction = new Wert.Action();