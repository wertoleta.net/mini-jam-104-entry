Wert.Enemy = function(Rect){
	Wert.Enemy.super.constructor.call(this, Rect, Wert.MovableType.Dynamic);
};

Extend(Wert.Enemy, Wert.RectangleBody);

Wert.Enemy.States = {
    Idle: 0,
    Dead: 1
};

Wert.Enemy.prototype._defaultSize = new Wert.FloatSize(32.0, 32.0);
Wert.Enemy.prototype._invincibleCounter = 0;
Wert.Enemy.prototype._invincibleCounterMax = 40;
Wert.Enemy.prototype._sprite = null;
Wert.Enemy.prototype._health = 0;
Wert.Enemy.prototype._state = 0;

Wert.Enemy.prototype.Update = function(){
	Wert.Enemy.super.Update.call(this);

    if (this._state == Wert.Enemy.States.Idle)
    {
        let hero = Wert.CurrentStage.hero;
        if (this.IsOverlaps(hero) && !hero.IsInvincible())
        {
            hero.Hurt(this);
        }
        if (this._invincibleCounter >= 0)
        {
            this._invincibleCounter--;        
            if (this._invincibleCounter == 0)
            {
                this.InvincibleEnd();
            }
        }

        if (this.y > Wert.CurrentStage.waterline.y)
        {
            this.Hurt(this);
        }
    }

    if (this.y > Wert.CurrentStage.BottomLine)
    {
        this.parent.removeChild(this);
        Wert.CurrentStage.phys.RemoveBody(this);
    }
};


Wert.Enemy.prototype.InvincibleEnd = function(){
};

Wert.Enemy.prototype.Hurt = function(source){
    if (this._health <= 0)
    {
        return false;
    }
    if (this._invincibleCounter <= 0)
    {
        this._invincibleCounter = this._invincibleCounterMax;
    }
    this._health--;
    PIXI.Loader.shared.resources["Kill"].data.currentTime = 0;
    PIXI.Loader.shared.resources["Kill"].data.play();
    if (this._health <= 0)
    {
        this._state = Wert.Enemy.States.Dead;
        Wert.CurrentStage.hero._score += 1000;
    }
    return true;
}