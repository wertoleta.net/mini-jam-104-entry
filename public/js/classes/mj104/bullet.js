Wert.Bullet = function(){
    let animation = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Fireball"];
    Wert.Bullet.super.constructor.call(this, animation);
    this._collisionBody = new Wert.RectangleBody({
        Position: new Wert.FloatPoint(0, 0),
        Size: new Wert.FloatSize(8, 8)
    });
    this.animationSpeed = 0.3;
    this.play();
};

Wert.Bullet.prototype._collisionBody = null;
Wert.Bullet.prototype._pendingKill = false;

Extend(Wert.Bullet, PIXI.AnimatedSprite);

Wert.Bullet.prototype.Update  = function(){
    Wert.Bullet.super.Update.call(this);

    if (this._pendingKill)
    {
        this.parent.removeChild(this);
        return;
    }

    this.position.x += 6 * this.scale.x;

    if (this.position.x < -100 || this.position.x >= Wert.ViewportSize.min.width + 100 || this.position.y > Wert.ViewportSize.min.height - Wert.CurrentStage.y)
    {
        this.parent.removeChild(this);
    }

    this._collisionBody.MoveTo(this.x - 4, this.y - 4);
    let collisions = Wert.CurrentStage.phys.GetAllOverlaped(this._collisionBody);
    collisions.forEach(collision => {
        if (collision._type == Wert.MovableType.Static && collision.IsSideSolid(Wert.Side.All)) {
            this._pendingKill = true;
        }
        if (collision instanceof Wert.Enemy){
            if (collision.Hurt(this))
            {
                this._pendingKill = true;
            }
        }
    });
};
