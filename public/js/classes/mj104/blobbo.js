Wert.Blobbo = function(Position){
    var rect = {Position: Position, Size: this._defaultSize};
	Wert.Blobbo.super.constructor.call(this, rect);
    let animation = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["SkBlobbo"];
    this._sprite = new PIXI.AnimatedSprite(animation);
    this._sprite.x = this.GetSize().Width / 2;
    this._sprite.y = this.GetSize().Height;
    this._sprite.animationSpeed = 0.2;
    this._sprite.scale.x = Math.random() >= 0.5 ? 1 : -1;
    this._sprite.play();
    this.addChild(this._sprite);

    this.SetVelocity(this._sprite.scale.x * this._speed, 0);

    this._invincibleCounterMax = 10;
    this._health = 3;

    this._forwardBody = new Wert.RectangleBody({
        Position: new Wert.FloatPoint(0, 0),
        Size: this._defaultSize
    });
};

Extend(Wert.Blobbo, Wert.Enemy);

Wert.Blobbo.prototype._defaultSize = new Wert.FloatSize(16.0, 24.0);
Wert.Blobbo.prototype._skateboardCreated = false;
Wert.Blobbo.prototype._speed = 3.0;
Wert.Blobbo.prototype._forwardBody = null;

Wert.Blobbo.prototype.Update = function(){
	Wert.Blobbo.super.Update.call(this);
    if (this._state == Wert.Enemy.States.Idle)
    {
        var isHoleForward = true;
        this._forwardBody.MoveTo(this.x + this._defaultSize.Width * this._sprite.scale.x, this.y + this._defaultSize.Height);
        let collisions = Wert.CurrentStage.phys.GetAllOverlaped(this._forwardBody);
        collisions.forEach(collision => {
            if (collision._type == Wert.MovableType.Static && collision.IsSideSolid(Wert.Side.Top)) {
                isHoleForward = false;
            }
        });

        if (this.IsSideTouched(Wert.Side.Horizontal))
        {
            this.AddVelocity(-this.GetVelocity().X, 0);
        }
        else if (this._sprite.scale.x == -1 && (this.IsSideTouched(Wert.Side.Left) || isHoleForward))
        {
            this.SetVelocity(this._speed, this.GetVelocity().Y);
            this._sprite.scale.x = 1;
        }
        else if (this._sprite.scale.x == 1 && (this.IsSideTouched(Wert.Side.Right) || isHoleForward))
        {
            this.SetVelocity(-this._speed, this.GetVelocity().Y);
            this._sprite.scale.x = -1;
        }
    }
    if (this._state == Wert.Enemy.States.Dead)
    {
        var currentVelocity = this.GetVelocity();
        if (Math.abs(currentVelocity.X) < 0.1)
        {
            this.SetVelocity(0.0, currentVelocity.Y);
        }
        else
        {
            var step = Math.max(Math.abs(currentVelocity.X) / 8.0, 0.1);
            this.AddVelocity(-step * Math.sign(currentVelocity.X), 0.0);
        }
        if (!this._skateboardCreated)
        {
            this._sprite.y = this.GetSize().Height * 1.5 + 2;

            var particle = new Wert.BaseParticle();
            particle.x = this.x;
            particle.y = this.y + 16;
            particle._velocity.x = 8 * this._sprite.scale.x;
            particle._velocity.y = -4;
            this._sprite.scale.x *= -1;
            var skateboard = new PIXI.Sprite(PIXI.Loader.shared.resources["Atlas"].textures["Skateboard"]);
            skateboard.anchor.set(0.5);
            particle.addChild(skateboard);
            Wert.CurrentStage.ParticleLayer.addChild(particle);
            this._skateboardCreated = true;
        }
    }
};

Wert.Blobbo.prototype.InvincibleEnd = function(){
    Wert.Blobbo.super.InvincibleEnd.call(this);

    this._sprite.textures = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["SkBlobbo"]
    this._sprite.play();
}

Wert.Blobbo.prototype.Hurt = function(source){
    if (Wert.Blobbo.super.Hurt.call(this, source))
    {
        if (this._health > 0)
        {
            this._sprite.textures = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["SkBlobboHurt"]
            this._sprite.play();
        }
        else
        {
            this._sprite.textures = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["SkBlobboDeath"];
            this._sprite.loop = false;
            this._sprite.play();
        }
        return true;
    }
    return false;
};