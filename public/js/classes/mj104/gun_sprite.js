Wert.GunSprite = function(){
    Wert.GunSprite.super.constructor.call(this, PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Weapon/Idle"]);
    this.animationSpeed = 0.15;
    this.play();
};

Extend(Wert.GunSprite, PIXI.AnimatedSprite);

Wert.GunSprite.prototype.Update = function()
{
    Wert.GunSprite.super.Update.call(this);

    if (!this.loop && this.currentFrame >= this.totalFrames - 1)
    {
        this.loop = true;
        this.animationSpeed = 0.15;
        this.textures = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Weapon/Idle"];
        this.play();
    }
}

Wert.GunSprite.prototype.Shoot = function()
{
    this.loop = false;
    this.animationSpeed = 0.3;
    this.textures = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Weapon/Attack"];
    this.play();
}