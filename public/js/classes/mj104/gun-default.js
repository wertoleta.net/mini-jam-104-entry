Wert.GunDefault = function(){
    this.CooldownTime = 30;
};

Extend(Wert.GunDefault, Wert.Gun);

Wert.GunDefault.prototype.Update = function(){
    Wert.GunDefault.super.Update.call(this);
}

Wert.GunDefault.prototype.Shoot = function(shooter){
    if (Wert.GunDefault.super.Shoot.call(this))
    {        
        var bullet = new Wert.Bullet();
        bullet.position.x = shooter.position.x + shooter.thisPhysicalOffset.X + 24 * shooter._sprite.scale.x;
        bullet.position.y = shooter.position.y + shooter.thisPhysicalOffset.Y - 10;
        bullet.scale.x = shooter._sprite.scale.x;
        Wert.CurrentStage.foreground.addChild(bullet);
        return true;
    }    
    return false;
}