
Wert.WaterFall = function(){
    let animation = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["W"];
	Wert.WaterFall.super.constructor.call(this, animation);
    this.animationSpeed = 0.3;
    this.play();
};

Extend(Wert.WaterFall, PIXI.AnimatedSprite);

Wert.WaterFall.prototype.Update = function(){
    Wert.WaterFall.super.Update.call(this);

    if (this.y > Wert.CurrentStage.BottomLine)
    {
        this.parent.removeChild(this);
    }
}