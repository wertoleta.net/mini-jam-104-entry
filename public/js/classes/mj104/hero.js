Wert.AdventurerState = {
        AS_NONE: -1,
        AS_IDLE: 0,
        AS_RUN: 1,
        AS_FALL: 2,
        AS_JUMP: 3,
        AS_CROUCH: 4,
        AS_SOMERSAULT: 5,
        AS_WALL_SLIDE: 6,
        AS_CORNER_GRAB: 7,
        AS_CORNER_CLIMB_UP: 8,
        AS_CORNER_CLIMB_DOWN: 9,
        AS_SLIDE: 10,
        AS_ATTACK_1: 11,
        AS_ATTACK_2: 12,
        AS_ATTACK_RUN: 13,
        AS_ATTACK_AIR_1: 14,
        AS_ATTACK_AIR_2: 15,
        AS_DEAD: 16
};

Wert.Hero = function(Position) {
    this.IdleSize = new Wert.FloatSize(16.0, 24.0);
    this.CrouchSize = new Wert.FloatSize(11.0, 12.0);
    var rect = {Position: Position, Size: this.IdleSize};
	Wert.Hero.super.constructor.call(this, rect, Wert.MovableType.Dynamic);
    
    Wert.Hero.prototype.thisPhysicalOffset = new Wert.FloatPoint(0, 0);
    Wert.Hero.prototype.thisAnimationOffset = new Wert.FloatPoint(0, 0);
    this._jumpTimer = 0;
    this.SetMaxVelocity(10.0, 12.0);

    this._sprite = new PIXI.AnimatedSprite(PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Player/Idle"]);
    
    this.addChild(this._sprite);

    this.SetState(Wert.AdventurerState.AS_IDLE);

    this._gunSprite = new Wert.GunSprite();
    this._sprite.addChild(this._gunSprite);
    this._score = 0;

    this._defaultGun = new Wert.GunDefault();
}

Extend(Wert.Hero, Wert.RectangleBody);

Wert.Hero.prototype.MaxJumpsCounter = 10;
Wert.Hero.prototype.SomersaultDelay = 20;
Wert.Hero.prototype.MinSlideSideArea = 9;
Wert.Hero.prototype.IdleSize = null;
Wert.Hero.prototype.CrouchSize = null;
Wert.Hero.prototype._state = Wert.AdventurerState.AS_IDLE;
Wert.Hero.prototype._nextState = Wert.AdventurerState.AS_IDLE;
Wert.Hero.prototype._jumpTimer = 0.0;
Wert.Hero.prototype._lastPressedDirection = 0;
Wert.Hero.prototype._accelerationTimer = 0.0;
Wert.Hero.prototype._somersaultTimer = 0.0;
Wert.Hero.prototype._slidedSide = 0;
Wert.Hero.prototype.thisPhysicalOffset = null;
Wert.Hero.prototype.thisAnimationOffset = null;
Wert.Hero.prototype._sprite = null;
Wert.Hero.prototype._defaultGun = null;
Wert.Hero.prototype._gun = null;
Wert.Hero.prototype._maxInvincibleTime = 90;
Wert.Hero.prototype._invincibleCounteer = 0;
Wert.Hero.prototype._health = 3;
Wert.Hero.prototype._maxHealth = 7;
Wert.Hero.prototype._gunSprite = null;

Wert.Hero.prototype.Update = function(timeScale)
{
    timeScale = 1;

    if (this._invincibleCounteer > 0)
    {
        this._invincibleCounteer--;
        this._sprite.alpha = Math.floor(this._invincibleCounteer / 3) % 2 == 0 ? 1 : 0.5;
    }
    else
    {
        this._sprite.alpha = 1;
    }

    this._sprite.position.x = this.thisPhysicalOffset.X + this.thisAnimationOffset.X;
    this._sprite.position.y = this.thisPhysicalOffset.Y + this.thisAnimationOffset.Y + 1;

    Wert.Hero.super.Update.call(this, timeScale);
    this.GetGun().Update();


    this._somersaultTimer = Math.max(this._somersaultTimer - timeScale, 0);
    this.ProcessMovementInput();
    switch (this._state)
    {
    case Wert.AdventurerState.AS_IDLE:
        this.ProcessIdle(timeScale);
        break;
    case Wert.AdventurerState.AS_RUN:
        this.ProcessRun(timeScale);
        break;
    case Wert.AdventurerState.AS_JUMP:
        this.ProcessJump(timeScale);
        break;
    case Wert.AdventurerState.AS_FALL:
        this.ProcessFall(timeScale);
        break;
    case Wert.AdventurerState.AS_CROUCH:
        this.ProcessJump(timeScale);
        break;
    case Wert.AdventurerState.AS_SOMERSAULT:
        this.ProcessSomersault(timeScale);
        break;
    case Wert.AdventurerState.AS_WALL_SLIDE:
        this.ProcessWallSlide(timeScale);
        break;
    case Wert.AdventurerState.AS_CORNER_GRAB:
        this.ProcessCornerGrab(timeScale);
        break;
    case Wert.AdventurerState.AS_CORNER_CLIMB_UP:
        this.ProcessCornerClimbUp(timeScale);
        break;
    case Wert.AdventurerState.AS_CORNER_CLIMB_DOWN:
        this.ProcessCornerClimbDown(timeScale);
        break;
    case Wert.AdventurerState.AS_SLIDE:
        this.ProcessSlide(timeScale);
        break;
    case Wert.AdventurerState.AS_ATTACK_1:
        this.ProcessAttack1(timeScale);
        break;
    case Wert.AdventurerState.AS_ATTACK_2:
        this.ProcessAttack2(timeScale);
        break;
    case Wert.AdventurerState.AS_ATTACK_RUN:
        this.ProcessAttackRun(timeScale);
        break;
    case Wert.AdventurerState.AS_ATTACK_AIR_1:
        this.ProcessAttackAir1(timeScale);
        break;
    case Wert.AdventurerState.AS_ATTACK_AIR_2:
        this.ProcessAttackAir2(timeScale);
        break;
    }

    this.SetState(this._nextState);

    if (this.y > Wert.CurrentStage.waterline.y + 16 || this.y > Wert.CurrentStage.BottomLine)
    {
        //this._health = 0;
        this.Hurt(this);
    }
}

Wert.Hero.prototype.ProcessAttack1 = function(timeScale)
{
    if (this.IsAnimationFinished())
    {
        if (Wert._attackAction.IsActive())
        {
            //if (this._lastPressedDirection == 0)
            //{
                this._nextState = Wert.AdventurerState.AS_ATTACK_2;
            //}
            //else
            //{
            //    this._nextState = Wert.AdventurerState.AS_ATTACK_RUN;
            //}
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_IDLE;
        }
    }
    this.ProcessFriction(timeScale);
}

Wert.Hero.prototype.ProcessAttack2 = function(timeScale)
{
    if (this.IsAnimationFinished())
    {
        if (Wert._attackAction.IsActive())
        {
            //if (this._lastPressedDirection == 0)
            //{
                this._nextState = Wert.AdventurerState.AS_ATTACK_1;
            //}
            //else
            //{
            //    this._nextState = Wert.AdventurerState.AS_ATTACK_RUN;
            //}
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_IDLE;
        }
    }
    this.ProcessFriction(timeScale);
}

Wert.Hero.prototype.ProcessAttackAir1 = function(timeScale)
{
    if (this.GetVelocity().Y > 0.0)
    {
        this.ProcessFall(timeScale);
    }
    else
    {
        this.ProcessJump(timeScale);
    }
    if (this._nextState == Wert.AdventurerState.AS_FALL || this._nextState == Wert.AdventurerState.AS_JUMP || this._nextState == Wert.AdventurerState.AS_ATTACK_AIR_1)
    {
        if (this.IsAnimationFinished())
        {
            if (Wert._attackAction.IsActive())
            {
                this._nextState = Wert.AdventurerState.AS_ATTACK_AIR_2;
            }
            else
            {
                this._nextState = Wert.AdventurerState.AS_JUMP;
            }
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_ATTACK_AIR_1;
        }
    }
}

Wert.Hero.prototype.ProcessAttackAir2 = function(timeScale)
{
    if (this.GetVelocity().Y > 0.0)
    {
        this.ProcessFall(timeScale);
    }
    else
    {
        this.ProcessJump(timeScale);
    }
    if (this._nextState == Wert.AdventurerState.AS_FALL || this._nextState == Wert.AdventurerState.AS_JUMP || this._nextState == Wert.AdventurerState.AS_ATTACK_AIR_1)
    {
        if (this.IsAnimationFinished())
        {
            if (Wert._attackAction.IsActive())
            {
                this._nextState = Wert.AdventurerState.AS_ATTACK_AIR_1;
            }
            else
            {
                this._nextState = Wert.AdventurerState.AS_JUMP;
            }
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_ATTACK_AIR_2;
        }
    }
}

Wert.Hero.prototype.ProcessAttackRun = function(timeScale)
{
    if (this.IsAnimationFinished())
    {
        if (Wert._attackAction.IsActive())
        {
            this.Attack();
            //if (this._lastPressedDirection == 0)
            //{
            //    this._nextState = Wert.AdventurerState.AS_ATTACK_1;
            //}
            //else
            //{
            //    this._nextState = Wert.AdventurerState.AS_RUN;
            //}
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_IDLE;
        }
    }
    this.ProcessFriction(timeScale / 3.0);
}

Wert.Hero.prototype.ProcessIdle = function(timeScale)
{
    if (Wert._downAction.IsFired())
    {
        var downBodies = this.GetTouchedBodies(Wert.Side.Bottom);
        var corners = this.GetCorners();
        this._slidedSide = Wert.Side.None;
        var downBody;
        for (var index = 0; index < downBodies.length; index++)
        {
            var downCorners = downBodies[index].GetCorners();
            if (corners.IsBetweenLeftAndRight(downCorners.Left))
            {
                this._slidedSide = Wert.Side.Right;
                downBody = downBodies[index];
                break;
            }
            else if (corners.IsBetweenLeftAndRight(downCorners.Right))
            {
                this._slidedSide = Wert.Side.Left;
                downBody = downBodies[index];
                break;
            }
        }
        if (this._slidedSide != Wert.Side.None)
        {
            var downBodyCorners = downBody.GetCorners();
            var tmpPosition = new Wert.FloatPoint(0.0, downBodyCorners.Top);
            if (this._slidedSide == Wert.Side.Right)
            {
                tmpPosition.X = downBodyCorners.Left - this.IdleSize.Width - 1;
            }
            else
            {
                tmpPosition.X = downBodyCorners.Right + 1;
            }
            var tmpBody = new Wert.RectangleBody({Position: tmpPosition, Size: this.IdleSize}, Wert.MovableType.Static);
            var overlappedBodies = Wert.CurrentStage.phys.GetAllOverlaped(tmpBody);
            if (overlappedBodies.length == 0)
            {
                this.MoveTo(tmpPosition.x, tmpPosition.y);
                this._nextState = Wert.AdventurerState.AS_CORNER_CLIMB_DOWN;
            }
        }
    }
    if (this._nextState != Wert.AdventurerState.AS_CORNER_CLIMB_DOWN)
    {
        this.ProcessMovement(timeScale);
        this.ProcessJump(timeScale);
    }
    if (this._nextState == Wert.AdventurerState.AS_IDLE && Wert._attackAction.IsActive())
    {
        this.Attack();
    }
}

Wert.Hero.prototype.ProcessRun = function(timeScale)
{
    if (Wert._downAction.IsActive())
    {
        this._nextState = Wert.AdventurerState.AS_SLIDE;
    }
    else
    {
        this.ProcessJump(timeScale);
    }
    if (this._nextState == Wert.AdventurerState.AS_RUN && Wert._attackAction.IsActive())
    {
        this.Attack();
        //this._nextState = Wert.AdventurerState.AS_ATTACK_RUN;
    }
}

Wert.Hero.prototype.ProcessCornerClimbUp = function(timeScale)
{
    this.SetVelocity(0.0, 0.0);
    if (this.IsAnimationFinished())
    {
        var size = this.GetSize();

        this.MoveOn((size.Width / 2.0) * Math.sign(this._sprite.scale.x), -size.Height - 1);
        this._nextState = Wert.AdventurerState.AS_IDLE;
    }
}

Wert.Hero.prototype.ProcessCornerClimbDown = function(timeScale)
{
    this.SetVelocity(0.0, 0.0);
    if (this.IsAnimationFinished())
    {
        var size = this.GetSize();
        ////this.MoveOn(copysignf(size.Width / 2.0f, GetScale().x), -size.Height - 1);
        this._nextState = Wert.AdventurerState.AS_CORNER_GRAB;
    }
}

Wert.Hero.prototype.ProcessCornerGrab = function(timeScale)
{
    this.SetVelocity(0.0, 0.0);
    if (Wert._downAction.IsActive())
    {
        this._nextState = Wert.AdventurerState.AS_WALL_SLIDE;
        //this.MoveOn(0.0, 6.0);
    }
    else if (Wert._upAction.IsActive())
    {
        if (Wert._rightAction.IsActive() && this.IsSideTouched(Wert.Side.Left))
        {
            this.DoWallJump(Wert.Side.Left);
        }
        else if (Wert._leftAction.IsActive() && this.IsSideTouched(Wert.Side.Right))
        {
            this.DoWallJump(Wert.Side.Right);
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_CORNER_CLIMB_UP;
        }
    }
}

Wert.Hero.prototype.ProcessWallSlide = function(timeScale)
{
    this.SetVelocity(0.0, 1.0);
    if (this.IsSideCollided(Wert.Side.Bottom))
    {
        this._nextState = Wert.AdventurerState.AS_IDLE;
    }
    var isSlideEnded = true;
    var touchedBodies = this.GetTouchedBodies(this._slidedSide);
    var top = this.GetCorners().Top;
    for (var i = 0; i < touchedBodies.length; i++)
    {
        var body = touchedBodies[i];
        var otherBottom = body.GetCorners().Bottom;
        var gap = otherBottom - top;
        if (gap >= this.MinSlideSideArea)
        {
            isSlideEnded = false;
            break;
        }
    }
    if (isSlideEnded || Wert._downAction.IsFired())
    {
        this._nextState = Wert.AdventurerState.AS_FALL;
        this._sprite.scale.x = this._slidedSide == Wert.Side.Left ? Math.abs(this._sprite.scale.x) : -Math.abs(this._sprite.scale.x);
    }
    else
    {
        if (Wert._upAction.IsActive())
        {
            this.DoWallJump(this._slidedSide);
        }
    }
    this.CheckFloor(timeScale);
}

Wert.Hero.prototype.DoWallJump = function(wallSide)
{
    if (wallSide == Wert.Side.Left)
    {
        this.SetVelocity(1.0, -3.0);
        this._jumpTimer = this.MaxJumpsCounter / 2.0;
        this._nextState = Wert.AdventurerState.AS_JUMP;
    }
    else if (wallSide == Wert.Side.Right)
    {
        this.SetVelocity(-1.0, -3.0);
        this._jumpTimer = this.MaxJumpsCounter / 2.0;
        this._nextState = Wert.AdventurerState.AS_JUMP;
    }
}

Wert.Hero.prototype.ProcessSomersault = function(timeScale)
{
    this.ProcessFriction(timeScale);
    var somerSaultSpeed = Math.abs(this.GetVelocity().X);
    this._animationSpeed = 0.3 * (somerSaultSpeed / (this.GetMaxVelocity().X / 2));
    if (somerSaultSpeed <= 0.5)
    {
        this._somersaultTimer = this.SomersaultDelay;
        this._nextState = Wert.AdventurerState.AS_CROUCH;
    }
    this.CheckFloor(timeScale);
}

Wert.Hero.prototype.ProcessFall = function(timeScale)
{
    var size = this.GetSize();
    var isSlideStarted = false;
    var isCornerGrabbed = false;
    var grabbedCorner;
    this._slidedSide = Wert.Side.None;
    if (this.IsSideCollided(Wert.Side.Left))
    {
        this._slidedSide = Wert.Side.Left;
    }
    else if (this.IsSideCollided(Wert.Side.Right))
    {
        this._slidedSide = Wert.Side.Right;
    }
    if (this._slidedSide != Wert.Side.None)
    {
        var touchedBodies = this.GetTouchedBodies(this._slidedSide);
        var top = this.GetCorners().Top;
        touchedBodies.forEach(body => {
            var otherCorners = body.GetCorners();
            var otherTop = otherCorners.Top;
            var otherBottom = otherCorners.Bottom;
            var topGap = top - otherTop;
            var bottomGap = otherBottom - top;
            if (topGap >= 1)
            {
                if (bottomGap >= this.MinSlideSideArea)
                {
                    isSlideStarted = true;
                }
            }
            else if (topGap >= -4)
            {
                isCornerGrabbed = true;
                grabbedCorner = body;
            }
        });
    }
    if (!isSlideStarted && !isCornerGrabbed)
    {
        this.ProcessJump(timeScale);
    }
    else
    {
        if (isCornerGrabbed)
        {
            var c = grabbedCorner.GetCorners();
            if (this._slidedSide == Wert.Side.Left)
            {
                //this.MoveTo(FloatPoint(c.Right + 1.0, c.Top));
            }
            else
            {
                //this.MoveTo(FloatPoint(c.Left - size.Width, c.Top));
            }
            this._nextState = Wert.AdventurerState.AS_CORNER_GRAB;
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_WALL_SLIDE;
        }
    }
}

Wert.Hero.prototype.CheckFloor = function(timeScale)
{
    if (!this.IsSideTouched(Wert.Side.Bottom)) 
    {
        if (this.GetVelocity().Y >= 0)
        {
            if (this._state == Wert.AdventurerState.AS_SOMERSAULT)
            {
                this.AddVelocity(-this.GetVelocity().X / 1.5, 0.0);
            }
            if (this._state == Wert.AdventurerState.AS_WALL_SLIDE)
            {
                this.AddVelocity(0.0, -0.3);
            }
            else
            {
                this._nextState = Wert.AdventurerState.AS_FALL;
            }
        }
        else if (this.GetVelocity().Y < 0.0)
        {
            this._nextState = Wert.AdventurerState.AS_JUMP;
        }
    }
    else if (this._state != Wert.AdventurerState.AS_JUMP)
    {
        this._jumpTimer = this.MaxJumpsCounter;
    }
    else if (this._nextState == Wert.AdventurerState.AS_JUMP)
    {
        this._nextState = Wert.AdventurerState.AS_IDLE;
    }
}

Wert.Hero.prototype.ProcessSlide = function(timeScale)
{
    var velocity = this.GetVelocity();
    if (Math.abs(velocity.X) < 0.5)
    {
        if (Wert._downAction.IsActive())
        {
            this._nextState = Wert.AdventurerState.AS_CROUCH;
        }
        else
        {
            this._nextState = Wert.AdventurerState.AS_RUN;
        }
    }
    this.ProcessFriction(timeScale / 3.0);
    this.CheckFloor(timeScale);
}

Wert.Hero.prototype.CheckJump = function(timeScale)
{

}

Wert.Hero.prototype.ProcessJump = function(timeScale)
{
    this.ProcessMovement(timeScale);
    if (this._jumpTimer > 0)
    {
        if (Wert._upAction.IsActive())
        {
            var verticalVelocityK = -1.4 * timeScale * Math.min(1.0, this._jumpTimer);
            if (this.MaxJumpsCounter - this._jumpTimer < 0.0001)
            {
                this.SetVelocity(this.GetVelocity().X, verticalVelocityK);
            }
            else
            {
                this.AddVelocity(0.0, verticalVelocityK);
            }
            if (!Wert.IsMuted)
            {
                PIXI.Loader.shared.resources["Jump"].data.play();
            }
        }
        else
        {
            this._jumpTimer = 0;
        }
        this._jumpTimer = Math.max(0.0, this._jumpTimer - timeScale);
    }
    this.CheckFloor(timeScale);
    this.CheckAirAttack();
}

Wert.Hero.prototype.CheckAirAttack = function()
{
    if ((this._nextState == Wert.AdventurerState.AS_JUMP || this._nextState == Wert.AdventurerState.AS_FALL) && Wert._attackAction.IsActive())
    {
        this.Attack();
        //this._nextState = Wert.AdventurerState.AS_ATTACK_AIR_1;
    }
}

Wert.Hero.prototype.SetState = function(newState)
{
    if (this._state == newState)
    {
        return;
    }
    var velocity = this.GetVelocity();
    var scale = this._sprite.scale;
    var newStateString = "";
    var initialOffset = new Wert.FloatPoint(this.thisPhysicalOffset.X, this.thisPhysicalOffset.Y);
    var _isNeedCompensateOffset = true;
    var scaleXK = this._slidedSide == Wert.Side.Left ? -1 : 1;
    this._sprite.loop = true;
    switch (newState)
    {
    case Wert.AdventurerState.AS_FALL:
        this._sprite.animationSpeed = 0.1;
        this.SwitchAnimation("Idle");
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Fall";
        break;
    case Wert.AdventurerState.AS_IDLE:
        this._sprite.animationSpeed = 0.1;
        this.SwitchAnimation("Idle");
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Idle";
        break;
    case Wert.AdventurerState.AS_JUMP:
        this._sprite.animationSpeed = 0.1;
        this.SwitchAnimation("Walk");
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Jump";
        break;
    case Wert.AdventurerState.AS_RUN:
        this._sprite.animationSpeed = 0.1;
        this.SwitchAnimation("Walk");
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Run";
        break;
    case Wert.AdventurerState.AS_CROUCH:
        return;
        this._sprite.animationSpeed = 0.1;
        this.SwitchAnimation("adventurer-crouch");
        this.SetSize(this.CrouchSize);
        this.thisPhysicalOffset.X = this.CrouchSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.CrouchSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Crouch";
        break;
    case Wert.AdventurerState.AS_SOMERSAULT:
        return;
        this._sprite.animationSpeed = 0.3;
        this.SwitchAnimation("adventurer-smrslt");
        this.SetSize(this.CrouchSize);
        this.thisPhysicalOffset.X = this.CrouchSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.CrouchSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Somersault";
        break;
    case Wert.AdventurerState.AS_WALL_SLIDE:
        return;
        this._sprite.animationSpeed = 0.1;
        this.SetVelocity(velocity.X, Math.min(velocity.Y, 0.5));
        this.SwitchAnimation("adventurer-wall-slide");
        newStateString = "WallSlide";
        this._sprite.scale.x = this._slidedSide == Wert.Side.Left ? -Math.abs(scale.x) : Math.abs(scale.x);
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        break;
    case Wert.AdventurerState.AS_CORNER_GRAB:
        return;
        this._sprite.animationSpeed = 0.1;
        this.SetVelocity(0.0, 0.0);
        this.SwitchAnimation("adventurer-crnr-grb");
        newStateString = "CornerGrab";

        this._sprite.scale.x = scaleXK * Math.abs(this._sprite.scale.x);
        this.SetSize(this.IdleSize);
        this.thisAnimationOffset.X = scaleXK * 1.4 * this.IdleSize.Width / 2.0;
        this.thisAnimationOffset.Y = -this.IdleSize.Height;
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        break;
    case Wert.AdventurerState.AS_CORNER_CLIMB_UP:
        return;
        this._sprite.animationSpeed = 0.2;
        this.SwitchAnimation("adventurer-crnr-clmb", false);
        newStateString = "CornerClimb";

        this.thisAnimationOffset.X = (1.4 * this.IdleSize.Width / 2.0) * Math.sign(scale.x);
        this.thisAnimationOffset.Y = -this.IdleSize.Height;
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.SetSize(this.IdleSize);
        this._sprite.loop = false;
        break;
    case Wert.AdventurerState.AS_CORNER_CLIMB_DOWN:
        return;
        this._sprite.animationSpeed = 0.2;
        this.SwitchAnimation("adventurer-crnr-clmb", false, true);
        newStateString = "CornerClimbDown";

        this._sprite.scale.x = scaleXK * Math.abs(this._sprite.scale.x);
        this.thisAnimationOffset.X = scaleXK * 1.4 * this.IdleSize.Width / 2.0;
        this.thisAnimationOffset.Y = -this.IdleSize.Height;
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.SetSize(this.IdleSize);
        this.SetVelocity(0.0, 0.0);
        this._sprite.loop = false;
        break;
    case Wert.AdventurerState.AS_SLIDE:
        return;
        this._sprite.animationSpeed = 0.3;
        this.SwitchAnimation("adventurer-slide");
        newStateString = "Slide";

        this.thisPhysicalOffset.X = this.CrouchSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.CrouchSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(10.0 * Math.sign(this._sprite.scale.x), 0.0);
        this.SetSize(this.CrouchSize);
        break;
    case Wert.AdventurerState.AS_ATTACK_1:
        this._sprite.animationSpeed = 0.3;
        this.SwitchAnimation("adventurer-attack1", false);
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Attack1";
        this._sprite.loop = false;
        break;
    case Wert.AdventurerState.AS_ATTACK_2:
        this._sprite.animationSpeed = 0.3;
        this.SwitchAnimation("adventurer-attack2", false);
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Attack2";
        this._sprite.loop = false;
        break;
    case Wert.AdventurerState.AS_ATTACK_RUN:
        return;
        this._sprite.animationSpeed = 0.3;
        this.SwitchAnimation("adventurer-attack3", false);
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "AttackRun";
        this.AddVelocity(Math.max(0.0, 1.5 - Math.abs(velocity.X)) * Math.sign(this._sprite.scale.x), 0.0);
        this._sprite.loop = false;
        break;
    case Wert.AdventurerState.AS_ATTACK_AIR_1:
        this._sprite.animationSpeed = 0.3;
        this.SwitchAnimation("adventurer-air-attack1", false);
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Air attack 1";
        if (velocity.Y > 0.0)
        {
            this.AddVelocity(-velocity.X / 2.0, -4.0);
        }
        this._sprite.loop = false;
        break;
    case Wert.AdventurerState.AS_ATTACK_AIR_2:
        this._sprite.animationSpeed = 0.3;
        this.SwitchAnimation("adventurer-air-attack2", false);
        this.SetSize(this.IdleSize);
        this.thisPhysicalOffset.X = this.IdleSize.Width / 2.0;
        this.thisPhysicalOffset.Y = this.IdleSize.Height;
        this.thisAnimationOffset = new Wert.FloatPoint(0.0, 0.0);
        newStateString = "Air attack 2";
        if (velocity.Y > 0.0)
        {
            this.AddVelocity(-velocity.X / 2.0, -4.0);
        }
        this._sprite.loop = false;
        break;
    case Wert.AdventurerState.AS_DEAD:
        this.SetSolidSides(Wert.Side.None);
        break;
    }
    //console.log("New state:" + newState);
    this._state = newState;
    this.MoveOn(initialOffset.X - this.thisPhysicalOffset.X, initialOffset.Y - this.thisPhysicalOffset.Y);
}

Wert.Hero.prototype.ProcessFriction = function(timeScale)
{
    var currentVelocity = this.GetVelocity();
    if (Math.abs(currentVelocity.X) < 0.1)
    {
        this.SetVelocity(0.0, currentVelocity.Y);
    }
    else
    {
        var step = Math.max(Math.abs(currentVelocity.X) / 8.0, 0.1);
        this.AddVelocity(-(step * timeScale) * Math.sign(currentVelocity.X), 0.0);
    }
}

Wert.Hero.prototype.ProcessMovementInput = function()
{
    if (!(Wert._leftAction.IsActive()) && !(Wert._rightAction.IsActive()))
    {
        this._lastPressedDirection = 0;
        this._accelerationTimer = 0;
    }
    else if (Wert._leftAction.IsFired())
    {
        this._lastPressedDirection = -1;
    }
    else if (Wert._rightAction.IsFired())
    {
        this._lastPressedDirection = 1;
    }
}

Wert.Hero.prototype.ProcessMovement = function(timeScale)
{
    var currentVelocity = this.GetVelocity();
    if (this._lastPressedDirection == 0)
    {
        if (this._state != Wert.AdventurerState.AS_JUMP)
        {
            if (Wert._downAction.IsActive())
            {
                this._nextState = Wert.AdventurerState.AS_CROUCH;
            }
            else
            {
                this._nextState = Wert.AdventurerState.AS_IDLE;
            }
        }
        this.ProcessFriction(timeScale);
    }
    else
    {
        var currentAcceleration = 0.0;
        if (!this.IsSideTouched(Wert.Side.Bottom) || this._state == Wert.AdventurerState.AS_JUMP)
        {
            this._accelerationTimer = timeScale;
            currentAcceleration = 1.0;
        }
        else
        {
            this._accelerationTimer += timeScale;
            currentAcceleration = Math.min(this._accelerationTimer / 2.0, 1.0);
        }
        if (this._state == Wert.AdventurerState.AS_CROUCH && Wert._downAction.IsActive())
        {
            this.ProcessFriction(timeScale);
            if (this._somersaultTimer == 0)
            {
                var maxControlledVelocity = (this.GetMaxVelocity().X / 2.0) * Math.sign(this._lastPressedDirection);
                var k = Math.max(Math.abs(maxControlledVelocity) * 2, 0.0);
                this.AddVelocity(k * this._lastPressedDirection * timeScale, 0.0);
                this._nextState = Wert.AdventurerState.AS_SOMERSAULT;
            }
        }
        else if (this._state != Wert.AdventurerState.AS_SOMERSAULT)
        {
            var maxControlledVelocity =(this.GetMaxVelocity().X / 2.0) * Math.sign(this._lastPressedDirection);
            var k = Math.max(Math.abs(maxControlledVelocity - currentVelocity.X), 0.0) / Math.abs(maxControlledVelocity);
            if (!this.IsSideTouched(Wert.Side.Bottom))
            {
                k /= 3.0;
            }
            this.AddVelocity(k * this._lastPressedDirection * currentAcceleration * timeScale, 0.0);
            if (this._state != Wert.AdventurerState.AS_JUMP)
            {
                this._nextState = Wert.AdventurerState.AS_RUN;
            }
        }
    }

    if (this._lastPressedDirection != 0)
    {
        this._sprite.scale.x = this._lastPressedDirection;
    }
}

Wert.Hero.prototype.SwitchAnimation = function(animationName)
{
    this._sprite.textures = PIXI.Loader.shared.resources["Atlas"].spritesheet.animations["Player/" + animationName];
    this._sprite.play();
}

Wert.Hero.prototype.IsAnimationFinished = function(){
    return this._sprite.currentFrame >= this._sprite.totalFrames - 1;
}

Wert.Hero.prototype.Attack = function(){
    var gun = this.GetGun();
    if (gun.ShootState != Wert.AdventurerState.AS_NONE)
    {
        this._nextState = gun.ShootState;
    }
    if (gun.Shoot(this))
    {
        this._gunSprite.Shoot();
    }
};

Wert.Hero.prototype.GetGun = function(){
    return this._gun == null ? this._defaultGun : this.gun;
};

Wert.Hero.prototype.Hurt = function(source){
    if (this._invincibleCounteer <= 0)
    {
        PIXI.Loader.shared.resources["Hit"].data.currentTime = 0;
        PIXI.Loader.shared.resources["Hit"].data.play();
        this.SetVelocity(40.0 * Math.sign(this.x - source.x), -4.0);
        this._invincibleCounteer = this._maxInvincibleTime;
        this._health--;
        Wert.CurrentStage.UpdateHearts();
        
        if (this._health <= 0)
        {
            this.SetState(Wert.AdventurerState.AS_DEAD);
            Wert.CurrentStage.EndLevel();
        }
    }
};

Wert.Hero.prototype.IsInvincible = function()
{
    return this._invincibleCounteer > 0;
}