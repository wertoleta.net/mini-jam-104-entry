Wert.Gun = function(){

};

Wert.Gun.prototype.IsReadyToShoot = false;
Wert.Gun.prototype.CooldownTime = 30;
Wert.Gun.prototype.CooldownCounter = 0;
Wert.Gun.prototype.ShootState = Wert.AdventurerState.AS_NONE;

Wert.Gun.prototype.Update = function(){
    if (this.CooldownCounter > 0)
    {
        this.CooldownCounter--;
    } 
    else  
    {
        this.IsReadyToShoot = true;
    }
}

Wert.Gun.prototype.Shoot = function(shooter){
    if (!this.IsReadyToShoot)
    {
        return false;
    }
    this.IsReadyToShoot = false;
    this.CooldownCounter = this.CooldownTime;
    if (!Wert.IsMuted)
    {
        PIXI.Loader.shared.resources["Shoot"].data.currentTime = 0;
        PIXI.Loader.shared.resources["Shoot"].data.play();
    }
    return true;
}