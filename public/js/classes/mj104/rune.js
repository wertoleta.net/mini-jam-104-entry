Wert.Rune = function() {
    Wert.Rune.super.constructor.call(this, PIXI.Loader.shared.resources["Atlas"].spritesheet.textures["Rune" + (1 + Math.floor(Math.random() * 4))]);
    this.dir = Math.random() < 0.5 ? 1 : -1;
    this.velocity = Math.random() * -this.dir;
};

Extend(Wert.Rune, PIXI.Sprite);

Wert.Rune.prototype.dir = 0;
Wert.Rune.prototype.velocity = 0;
Wert.Rune.prototype._collisionBody = new Wert.RectangleBody({
    Position: new Wert.FloatPoint(0, 0),
    Size: new Wert.FloatSize(8, 8)
});

Wert.Rune.prototype.Update = function(){
    Wert.Rune.super.Update.call(this);

    this.anchor.y += this.velocity / this.height;

    this.velocity += this.dir / 10;

    if (this.velocity < -1)
    {
        this.velocity = -1;
        this.dir = 1;
    } else if (this.velocity > 1)
    {
        this.velocity = 1;
        this.dir = -1;
    }

    if (this.y > Wert.CurrentStage.BottomLine)
    {
        this.parent.removeChild(this);
    }

    this._collisionBody.MoveTo(this.x - 4, this.y - 4);
    let collisions = Wert.CurrentStage.phys.GetAllOverlaped(this._collisionBody);
    collisions.forEach(collision => {
        if (collision instanceof Wert.Hero){
            this.parent.removeChild(this);
            collision._score += 100;
            var name = "Coin" + (3 + Math.floor(Math.random() * 2));
            PIXI.Loader.shared.resources[name].data.currentTime = 0;
            PIXI.Loader.shared.resources[name].data.play();
        }
    });
};