Wert.BubbleChild = function() {
	Wert.BubbleChild.super.constructor.call(this);
	
	this.face = new PIXI.Sprite(this.SmileFaceTexture);
	this.face.anchor.set(0.5);
	
	this.eyelids = new PIXI.Sprite(this.EyelidsTexture);
	this.eyelids.visible = false;
	this.eyelids.anchor.set(0.5);
	
	this.eyelidsTimer = 0;
	this.shootCooldown = 0;
	this.state = Wert.BubbleHero.prototype.EState.Idle;
	this.upSpeed = 0;
	this.canHandleItem = false;
	this.isEmergeable = false;
	this.upSpeed = -1;
	
	this.SetArea(this.MediumSizeArea);
	
	this.addChild(this.face);
	this.addChild(this.eyelids);
};

Extend(Wert.BubbleChild, Wert.Bubble);

Wert.BubbleChild.prototype.SmileFaceTexture = null;
Wert.BubbleChild.prototype.EyelidsTexture = null;

Wert.BubbleChild.prototype.eyelids = null;
Wert.BubbleChild.prototype.eyelidsTimer = 0;
Wert.BubbleChild.prototype.face = null;

Wert.BubbleChild.prototype.Update = function(){
	Wert.BubbleChild.super.Update.call(this);
	
	this.eyelidsTimer--;
	if (this.eyelidsTimer <= 0) 
	{
		this.eyelidsTimer = this.eyelids.visible ? 60 + Math.floor(Math.random() * 30) : 10;
		this.eyelids.visible = !this.eyelids.visible;
	}
}